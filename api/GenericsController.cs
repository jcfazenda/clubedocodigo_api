﻿
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using api.Controllers.Generics;
using api.Domain.Repository.Interface.Empresas;
using api.Domain.Repository.Interface.Estados;
using api.Domain.Repository.Interface.PerfisAcesso.Telas;
using api.Domain.Repository.Interface.Usuario;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{

    [EnableCors("CorsPolicy")]
    [Produces("application/json")]
    [Route("{tenant_database}/api/Generics")]
    public class GenericsController : Controller
    {
        private readonly IUsuariosRepository _usuarios;
        private readonly IEmpresaClassificacaoRepository _empresaClassificacao;
        private readonly IEstadoRepository _estado;
        private readonly INivelAcessoRepository _nivelAcesso;
        private readonly IUsuarioPerfilRepository _usuarioPerfil;

        public GenericsController(IUsuariosRepository usuarios, 
                                  IEmpresaClassificacaoRepository empresaClassificacao,
                                  IEstadoRepository estado,
                                  INivelAcessoRepository nivelAcesso, IUsuarioPerfilRepository usuarioPerfil)
        {
            _nivelAcesso                            = nivelAcesso;
            _empresaClassificacao                   = empresaClassificacao;
            _estado                                 = estado;
            _usuarios                               = usuarios;
            _usuarioPerfil                          = usuarioPerfil;
        }

        [HttpPost("GetList")]
        [EnableCors("CorsPolicy")]
        public async Task<ListGenericsOutput> GetList([FromBody] List<ListGenericsInput> input)
        {
            ListGenericsOutput ListReturn = new ListGenericsOutput();

            try
            {

                if (ModelState.IsValid)
                {
                    foreach (var item in input)
                    {
                        switch (item.Nome)
                        {
                            case "UsuarioPerfil":
                                ListReturn.ListUsuarioPerfil = ListItems.ListUsuarioPerfilSet(_usuarioPerfil.GetAll(true).ToList(), 0);
                                break;

                            case "NivelAcesso":
                                ListReturn.ListNivelAcesso = ListItems.ListNivelAcessoSet(_nivelAcesso.GetAll(true).ToList(), 0);
                                break;

                            case "Usuarios":
                                ListReturn.ListUsuarios = ListItems.ListUsuariosSet(_usuarios.GetAll(true).ToList(), 0);
                                break; 

                            case "Estados":
                                ListReturn.ListEstados = ListItems.ListEstadosSet(_estado.GetAll(true).ToList(), 0);
                                break;

                            case "TipoLogradouro":
                                ListReturn.ListTipoLogradouro = ListItems.ListTipoLogradouro();
                                break;

                            case "UF":
                                ListReturn.ListUF = ListItems.ListUF();
                                break;

                            case "EmpresaClassificacao":
                                ListReturn.ListEmpresaClassificacao = ListItems.ListEmpresaClassificacaoSet(_empresaClassificacao.GetAll(true).ToList(), 0);
                                break;

                            default:
                                break;
                        }
                    }
                }

                await Task.Delay(1);
                return ListReturn; 

            }
            catch (System.Exception )
            {
                return ListReturn;
            }
        }

        [HttpPost("GetEnderecoCorreios")]
        [EnableCors("CorsPolicy")]
        public async Task<IActionResult> GetEnderecoCorreios([FromBody] string Cep)
        {
            await Task.Delay(1);

            try
            {

                Regex regexObj = new Regex(@"[^\d]");
                Cep = regexObj.Replace(Cep, "");

                var end = new CepConsulta
                {
                    Cep = Cep
                };

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.buscacep.correios.com.br/sistemas/buscacep/resultadoBuscaCepEndereco.cfm");
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                byte[] postBytes = Encoding.ASCII.GetBytes("relaxation=" + Cep.Replace("-", "") + "&tipoCEP=ALL&semelhante=N");

                request.GetRequestStream()
                    .Write(postBytes, 0, postBytes.Length);

                string responseText = new StreamReader(request.GetResponse().GetResponseStream(), Encoding.GetEncoding("ISO-8859-1")).ReadToEnd();

                string s = responseText.Substring(responseText.LastIndexOf("<tr>"));
                s = s.Substring(0, s.LastIndexOf("</tr>"));
                s = s.Replace("<tr>", "");
                s = s.Replace("</tr>", "");
                s = s.Replace("&nbsp;", "");
                s = s.Replace("\r", "\n");

                List<string> Address = new List<string>();
                const string pattern = @"<td\b[^>]*?>(?<V>[\s\S]*?)</\s*td>";
                foreach (Match match in Regex.Matches(s, pattern, RegexOptions.IgnoreCase))
                {
                    string value = match.Groups["V"].Value;
                    Address.Add(value);
                }

                end.Rua = Address[0];
                end.Bairro = Address[1];
                end.Cidade = Address[2].Substring(0, Address[2].LastIndexOf("/"));
                end.UF = Address[2].Substring(Address[2].LastIndexOf("/") + 1);

                if (Address[0] == "")
                    end.universalCep = true;

                return Response(end, "success");

            }
            catch (System.Exception ex)
            {
                return Response("Não foi possível localizar pelo Cep: " + Cep, ex.Message);
            }
        }

        protected new ActionResult Response(object result,  string message)
        {
            return Ok(new
            {
                success = true,
                data = result,
                message
            });
        }
    }
}
