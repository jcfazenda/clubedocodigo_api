﻿using System.Collections.Generic;
using api.Domain.Models.Usuario;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace api.Controllers.Generics
{
    public class ListItems
    {
        public long Value { get; set; }
        public string Text { get; set; }

        public static List<SelectListItem> ListUsuariosSet(List<Usuarios> Usuarios, long id)
        {
            bool Selected = false;

            List<SelectListItem> List = new List<SelectListItem>();

            foreach (var item in Usuarios)
            {
                Selected = false;
                if (id == item.Id_Usuario) { Selected = true; }

                List.Add(new SelectListItem
                {
                    Text = item.Usuario_Nome + " " + item.Usuario_Sobrenome,
                    Value = item.Id_Usuario.ToString(),
                    Selected = Selected
                });
            }

            return List;
        }

    }
}


