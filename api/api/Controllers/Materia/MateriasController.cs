﻿using api.Domain.Repository.Interface.Materiass;
using api.Domain.Views.Input.Materia;
using api.Domain.Views.Output.Materia;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers.Materiass
{
    [EnableCors("CorsPolicy")]

    [Produces("application/json")]
    [Route("{tenant_database}/api/Materias")]
    public class MateriasController : Controller
    {
        private readonly IMateriasRepository _Materias; 

        public MateriasController(IMateriasRepository Materias)
        {
            _Materias = Materias;
        }

        [HttpPost("Save")]
        [EnableCors("CorsPolicy")]
        public IActionResult Save([FromBody] MateriasInput input)
        {
            if (input == null) { return Response(true, "Alerta", "não foi possível executar esta tarefa.", null, "warn"); }

            if (input.Id_Materia  > 0) { _Materias.Update(input); }
            if (input.Id_Materia == 0) { _Materias.Create(input); }

            return Response(true, "Sucesso", "base de dados atuaalizada com sucesso", null, "success");
        }

        [HttpPost("GetAny")]
        [EnableCors("CorsPolicy")]
        public IActionResult GetAny([FromBody] MateriasInput input)
        {
            var data = _Materias.GetAny((bool)input.Fl_Ativo).ProjectTo<MateriasOutput>();

            return Response(true, "Sucesso", "..", data, "success");
        }

        [HttpPost("GetById")]
        [EnableCors("CorsPolicy")]
        public IActionResult GetById([FromBody] MateriasInput input)
        {
            var data = _Materias.GetById(input.Id_Materia).ProjectTo<MateriasOutput>();

            return Response(true, "Sucesso", "..", data, "success");
        }


        [HttpPost("UpdateStatus")]
        [EnableCors("CorsPolicy")]
        public IActionResult UpdateStatus([FromBody] MateriasInput input)
        {
            var data = _Materias.UpdateStatus(input.Id_Materia);

            return Response(true, "Sucesso", "Registro atualizado com sucesso", data, "success");
        }

        [HttpPost("Remove")]
        [EnableCors("CorsPolicy")]
        public IActionResult Remove([FromBody] MateriasInput input)
        {
            _Materias.Remove(input.Id_Materia);

            return Response(true, "Sucesso", "Registro removido com sucesso", null, "success");
        }

        protected new ActionResult Response(bool success, string Title, string Message, object data, string type)
        {
            return Ok(new
            {
                success,
                Title,
                Message,
                data,
                type
            });
        }

    }
}
