﻿
namespace Api.Domain.Configure
{
    using api.Domain.Repository.Interface.Materiass;
    using api.Domain.Repository.Queryable.Materiass;
    using Microsoft.Extensions.DependencyInjection;

    public class UserNativeInjection
    {
        public static void RegisterServices(IServiceCollection services)
        {
            RegisterUserServices(services);
            RegisterRepositories(services);
            RegisterCommands(services);
        }

        private static void RegisterRepositories(IServiceCollection services)
        {
            /* Materias */
            services.AddScoped<IMateriasRepository, MateriasRepository>();


        }

        private static void RegisterUserServices(IServiceCollection services)
        {
        }

        private static void RegisterCommands(IServiceCollection services)
        {
        }
    }
}
