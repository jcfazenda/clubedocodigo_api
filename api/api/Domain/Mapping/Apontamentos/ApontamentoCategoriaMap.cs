﻿using api.Domain.Models.Apontamentos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Apontamentos
{
    public sealed class ApontamentoCategoriaMap : IEntityTypeConfiguration<ApontamentoCategoria>
    {

        public void Configure(EntityTypeBuilder<ApontamentoCategoria> constuctor)
        {

            constuctor.ToTable("apontamento_categoria");

            constuctor.Property(m => m.Id_Apontamento_Categoria).HasColumnName("Id_Apontamento_Categoria").IsRequired();
            constuctor.HasKey(o => o.Id_Apontamento_Categoria);

            constuctor.Property(m => m.Apontamento_Categoria_Nome).HasColumnName("Apontamento_Categoria_Nome");

        }
    }
}
