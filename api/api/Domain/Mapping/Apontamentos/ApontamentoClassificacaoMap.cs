﻿using api.Domain.Models.Apontamentos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Apontamentos
{
    public sealed class ApontamentoClassificacaoMap : IEntityTypeConfiguration<ApontamentoClassificacao>
    {

        public void Configure(EntityTypeBuilder<ApontamentoClassificacao> constuctor)
        {

            constuctor.ToTable("apontamento_classificacao");

            constuctor.Property(m => m.Id_Apontamento_Classificacao).HasColumnName("Id_Apontamento_Classificacao").IsRequired();
            constuctor.HasKey(o => o.Id_Apontamento_Classificacao);

            constuctor.Property(m => m.Apontamento_Classificacao_Nome).HasColumnName("Apontamento_Classificacao_Nome");
            constuctor.Property(m => m.Icon).HasColumnName("Icon");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
