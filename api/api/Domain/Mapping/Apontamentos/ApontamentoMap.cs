﻿using api.Domain.Models.Apontamentos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Apontamentos
{
    public sealed class ApontamentoMap : IEntityTypeConfiguration<Apontamento>
    {

        public void Configure(EntityTypeBuilder<Apontamento> constuctor)
        {

            constuctor.ToTable("apontamento");

            constuctor.Property(m => m.Id_Apontamento).HasColumnName("Id_Apontamento").IsRequired();
            constuctor.HasKey(o => o.Id_Apontamento);

            constuctor.Property(m => m.Id_Apontamento_Classificacao).HasColumnName("Id_Apontamento_Classificacao");
            constuctor.Property(m => m.Id_Apontamento_Categoria).HasColumnName("Id_Apontamento_Categoria");

            constuctor.Property(m => m.Apontamento_Nome).HasColumnName("Apontamento_Nome");
            constuctor.Property(m => m.Apontamento_Descricao).HasColumnName("Apontamento_Descricao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
            constuctor.Property(m => m.Fl_Pontecial_Saving).HasColumnName("Fl_Pontecial_Saving");
            constuctor.Property(m => m.Apontamento_Descricao_Saving).HasColumnName("Apontamento_Descricao_Saving");
            constuctor.Property(m => m.Valor_Saving).HasColumnName("Valor_Saving");

            constuctor.HasOne(m => m.ApontamentoCategoria).WithMany(m => m.Apontamento)
                                                          .HasForeignKey(x => x.Id_Apontamento_Categoria).HasPrincipalKey(x => x.Id_Apontamento_Categoria); /* Join */

            constuctor.HasOne(m => m.ApontamentoClassificacao).WithMany(m => m.Apontamento)
                                                              .HasForeignKey(x => x.Id_Apontamento_Classificacao).HasPrincipalKey(x => x.Id_Apontamento_Classificacao); /* Join */

        }
    }
}
