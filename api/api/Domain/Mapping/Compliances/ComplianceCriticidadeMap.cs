﻿using api.Domain.Models.Compliances;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Compliances
{
    public sealed class ComplianceCriticidadeMap : IEntityTypeConfiguration<ComplianceCriticidade>
    {

        public void Configure(EntityTypeBuilder<ComplianceCriticidade> constuctor)
        {

            constuctor.ToTable("compliance_criticidade");

            constuctor.Property(m => m.Id_Compliance_Criticidade).HasColumnName("Id_Compliance_Criticidade").IsRequired();
            constuctor.HasKey(o => o.Id_Compliance_Criticidade);

            constuctor.Property(m => m.Compliance_Criticidade_Nome).HasColumnName("Compliance_Criticidade_Nome");
        }
    }
}
