﻿using api.Domain.Models.Compliances;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Compliances
{
    public sealed class ComplianceNormaEmpresaContraparteMap : IEntityTypeConfiguration<ComplianceNormaEmpresaContraparte>
    {

        public void Configure(EntityTypeBuilder<ComplianceNormaEmpresaContraparte> constuctor)
        {

            constuctor.ToTable("compliance_norma_empresa_contraparte");

            constuctor.Property(m => m.Id_Compliance_Norma_Empresa_Contraparte).HasColumnName("Id_Compliance_Norma_Empresa_Contraparte").IsRequired();
            constuctor.HasKey(o => o.Id_Compliance_Norma_Empresa_Contraparte);

            constuctor.Property(m => m.Id_Empresa).HasColumnName("Id_Empresa");
            constuctor.Property(m => m.Id_Compliance_Norma_Empresa).HasColumnName("Id_Compliance_Norma_Empresa");

            constuctor.HasOne(m => m.Empresa).WithMany(m => m.ComplianceNormaContraparte)
                      .HasForeignKey(x => x.Id_Empresa).HasPrincipalKey(x => x.Id_Empresa); /* Join */ 

        }
    }
}
