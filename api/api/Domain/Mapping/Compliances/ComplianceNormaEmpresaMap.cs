﻿using api.Domain.Models.Compliances;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Compliances
{
    public sealed class ComplianceNormaEmpresaMap : IEntityTypeConfiguration<ComplianceNormaEmpresa>
    {

        public void Configure(EntityTypeBuilder<ComplianceNormaEmpresa> constuctor)
        {

            constuctor.ToTable("compliance_norma_empresa");

            constuctor.Property(m => m.Id_Compliance_Norma_Empresa).HasColumnName("Id_Compliance_Norma_Empresa").IsRequired();
            constuctor.HasKey(o => o.Id_Compliance_Norma_Empresa);

            constuctor.Property(m => m.Id_Empresa).HasColumnName("Id_Empresa");
            constuctor.Property(m => m.Id_Compliance_Norma).HasColumnName("Id_Compliance_Norma");

            constuctor.HasOne(m => m.Empresa).WithMany(m => m.ComplianceNormaEmpresa)
                      .HasForeignKey(x => x.Id_Empresa).HasPrincipalKey(x => x.Id_Empresa); /* Join */


        }
    }
}
