﻿using api.Domain.Models.Compliances;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Compliances
{
    public sealed class ComplianceNormaMap : IEntityTypeConfiguration<ComplianceNorma>
    {

        public void Configure(EntityTypeBuilder<ComplianceNorma> constuctor)
        {

            constuctor.ToTable("compliance_norma");

            constuctor.Property(m => m.Id_Compliance_Norma).HasColumnName("Id_Compliance_Norma").IsRequired();
            constuctor.HasKey(o => o.Id_Compliance_Norma);

            constuctor.Property(m => m.Id_Compliance_Norma_Tipo).HasColumnName("Id_Compliance_Norma_Tipo");
            constuctor.Property(m => m.Id_Orgao_Regulador).HasColumnName("Id_Orgao_Regulador");
            constuctor.Property(m => m.Id_Norma_Criticidade).HasColumnName("Id_Norma_Criticidade");
            constuctor.Property(m => m.Id_Periodo_Revisao).HasColumnName("Id_Periodo_Revisao");
            constuctor.Property(m => m.Id_Usuario_Gestor).HasColumnName("Id_Usuario_Gestor");

            constuctor.Property(m => m.Data_Publicacao).HasColumnName("Data_Publicacao");
            constuctor.Property(m => m.Data_Inicio_Vigencia).HasColumnName("Data_Inicio_Vigencia");
            constuctor.Property(m => m.Data_Proxima_Revisao).HasColumnName("Data_Proxima_Revisao");
            constuctor.Property(m => m.Compliance_Norma_Nome).HasColumnName("Compliance_Norma_Nome");

            constuctor.Property(m => m.Compliance_Norma_Descricao).HasColumnName("Compliance_Norma_Descricao");
            constuctor.Property(m => m.Compliance_Norma_Conclusao_Analise).HasColumnName("Compliance_Norma_Conclusao_Analise");
            constuctor.Property(m => m.Compliance_Anexo_Byte).HasColumnName("Compliance_Anexo_Byte");
            constuctor.Property(m => m.Compliance_Anexo_Nome).HasColumnName("Compliance_Anexo_Nome");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo"); 
            constuctor.Property(m => m.Fl_Revisao_Manual).HasColumnName("Fl_Revisao_Manual"); 


            constuctor.HasOne(m => m.ComplianceNormaTipo).WithMany(m => m.ComplianceNorma)
                      .HasForeignKey(x => x.Id_Compliance_Norma_Tipo).HasPrincipalKey(x => x.Id_Compliance_Norma_Tipo); /* Join */

            constuctor.HasOne(m => m.OrgaoRegulador).WithMany(m => m.ComplianceNorma)
                      .HasForeignKey(x => x.Id_Orgao_Regulador).HasPrincipalKey(x => x.Id_Orgao_Regulador); /* Join */


        }
    }
}