﻿using api.Domain.Models.Compliances;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Compliances
{
    public sealed class ComplianceNormaTipoMap : IEntityTypeConfiguration<ComplianceNormaTipo>
    {

        public void Configure(EntityTypeBuilder<ComplianceNormaTipo> constuctor)
        {

            constuctor.ToTable("compliance_norma_tipo");

            constuctor.Property(m => m.Id_Compliance_Norma_Tipo).HasColumnName("Id_Compliance_Norma_Tipo").IsRequired();
            constuctor.HasKey(o => o.Id_Compliance_Norma_Tipo);

            constuctor.Property(m => m.Compliance_Norma_Tipo_Nome).HasColumnName("Compliance_Norma_Tipo_Nome"); 
        }
    }
}
