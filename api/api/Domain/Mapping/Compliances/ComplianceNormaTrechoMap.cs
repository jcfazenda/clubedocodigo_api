﻿using api.Domain.Models.Compliances;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Compliances
{
    public sealed class ComplianceNormaTrechoMap : IEntityTypeConfiguration<ComplianceNormaTrecho>
    {

        public void Configure(EntityTypeBuilder<ComplianceNormaTrecho> constuctor)
        {

            constuctor.ToTable("compliance_norma_trecho");

            constuctor.Property(m => m.Id_Compliance_Norma_Trecho).HasColumnName("Id_Compliance_Norma_Trecho").IsRequired();
            constuctor.HasKey(o => o.Id_Compliance_Norma_Trecho);

            constuctor.Property(m => m.Id_Compliance_Norma).HasColumnName("Id_Compliance_Norma");
            constuctor.Property(m => m.Compliance_Norma_Trecho_Nome).HasColumnName("Compliance_Norma_Trecho_Nome");
            constuctor.Property(m => m.Compliance_Norma_Trecho_Descricao).HasColumnName("Compliance_Norma_Trecho_Descricao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.ComplianceNorma).WithMany(m => m.ComplianceNormaTrecho)
                                    .HasForeignKey(x => x.Id_Compliance_Norma).HasPrincipalKey(x => x.Id_Compliance_Norma); /* Join */


        }
    }
}
