﻿using api.Domain.Models.Compliances;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Compliances
{
    public sealed class ComplianceNormaTrechoRiscoMap : IEntityTypeConfiguration<ComplianceNormaTrechoRisco>
    {

        public void Configure(EntityTypeBuilder<ComplianceNormaTrechoRisco> constuctor)
        {

            constuctor.ToTable("compliance_norma_trecho_risco");

            constuctor.Property(m => m.Id_Compliance_Norma_Trecho_Risco).HasColumnName("Id_Compliance_Norma_Trecho_Risco").IsRequired();
            constuctor.HasKey(o => o.Id_Compliance_Norma_Trecho_Risco);

            constuctor.Property(m => m.Id_Compliance_Norma_Trecho).HasColumnName("Id_Compliance_Norma_Trecho"); 
            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");

            constuctor.HasOne(m => m.Riscos).WithMany(m => m.ComplianceNormaTrechoRisco)
                                    .HasForeignKey(x => x.Id_Risco).HasPrincipalKey(x => x.Id_Risco); /* Join */


        }
    }
}
