﻿using api.Domain.Models.Compliances;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Compliances
{
    public sealed class CompliancePeriodoRevisaoMap : IEntityTypeConfiguration<CompliancePeriodoRevisao>
    {

        public void Configure(EntityTypeBuilder<CompliancePeriodoRevisao> constuctor)
        {

            constuctor.ToTable("compliance_periodo_revisao");

            constuctor.Property(m => m.Id_Compliance_Periodo_Revisao).HasColumnName("Id_Compliance_Periodo_Revisao").IsRequired();
            constuctor.HasKey(o => o.Id_Compliance_Periodo_Revisao);

            constuctor.Property(m => m.Compliance_Periodo_Revisao_Nome).HasColumnName("Compliance_Periodo_Revisao_Nome");
        }
    }
}
