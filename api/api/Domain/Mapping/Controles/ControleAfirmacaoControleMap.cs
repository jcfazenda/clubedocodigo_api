﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleAfirmacaoControleMap : IEntityTypeConfiguration<ControleAfirmacaoControle>
    {

        public void Configure(EntityTypeBuilder<ControleAfirmacaoControle> constuctor)
        {

            constuctor.ToTable("controle_afirmacao_controle");

            constuctor.Property(m => m.Id_Controle_Afirmacao_Controle).HasColumnName("Id_Controle_Afirmacao_Controle").IsRequired();
            constuctor.HasKey(o => o.Id_Controle_Afirmacao_Controle);

            constuctor.Property(m => m.Id_Controle).HasColumnName("Id_Controle");
            constuctor.Property(m => m.Id_Controle_Afirmacao).HasColumnName("Id_Controle_Afirmacao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.ControleAfirmacao).WithMany(m => m.ControleAfirmacaoControle)
                                .HasForeignKey(x => x.Id_Controle_Afirmacao).HasPrincipalKey(x => x.Id_Controle_Afirmacao); /* Join */

        }
    }
}
