﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleAfirmacaoMap : IEntityTypeConfiguration<ControleAfirmacao>
    {

        public void Configure(EntityTypeBuilder<ControleAfirmacao> constuctor)
        {

            constuctor.ToTable("controle_afirmacao");

            constuctor.Property(m => m.Id_Controle_Afirmacao).HasColumnName("Id_Controle_Afirmacao").IsRequired();
            constuctor.HasKey(o => o.Id_Controle_Afirmacao);

            constuctor.Property(m => m.Controle_Afirmacao_Nome).HasColumnName("Controle_Afirmacao_Nome");
            constuctor.Property(m => m.Controle_Afirmacao_Descricao).HasColumnName("Controle_Afirmacao_Descricao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo"); 
        }
    }
}
