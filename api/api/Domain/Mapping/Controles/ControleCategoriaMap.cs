﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleCategoriaMap : IEntityTypeConfiguration<ControleCategoria>
    {

        public void Configure(EntityTypeBuilder<ControleCategoria> constuctor)
        {

            constuctor.ToTable("controle_categoria");

            constuctor.Property(m => m.Id_Controle_Categoria).HasColumnName("Id_Controle_Categoria").IsRequired();
            constuctor.HasKey(o => o.Id_Controle_Categoria);

            constuctor.Property(m => m.Controle_Categoria_Nome).HasColumnName("Controle_Categoria_Nome"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
        }
    }
}
