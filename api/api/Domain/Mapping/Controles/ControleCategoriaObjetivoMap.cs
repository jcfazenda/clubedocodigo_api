﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleCategoriaObjetivoMap : IEntityTypeConfiguration<ControleCategoriaObjetivo>
    {

        public void Configure(EntityTypeBuilder<ControleCategoriaObjetivo> constuctor)
        {

            constuctor.ToTable("controle_categoria_objetivo");

            constuctor.Property(m => m.Id_Controle_Categoria_Objetivo).HasColumnName("Id_Controle_Categoria_Objetivo").IsRequired();
            constuctor.HasKey(o => o.Id_Controle_Categoria_Objetivo);

            constuctor.Property(m => m.Controle_Categoria_Objetivo_Nome).HasColumnName("Controle_Categoria_Objetivo_Nome");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
        }
    }
}
