﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleDemonstracaoFinanceiraControleMap : IEntityTypeConfiguration<ControleDemonstracaoFinanceiraControle>
    {

        public void Configure(EntityTypeBuilder<ControleDemonstracaoFinanceiraControle> constuctor)
        {

            constuctor.ToTable("controle_demonstracao_financeira_controle");

            constuctor.Property(m => m.Id_Controle_Demonstracao_Financeira_Controle).HasColumnName("Id_Controle_Demonstracao_Financeira_Controle").IsRequired();
            constuctor.HasKey(o => o.Id_Controle_Demonstracao_Financeira_Controle);

            constuctor.Property(m => m.Id_Controle).HasColumnName("Id_Controle");
            constuctor.Property(m => m.Id_Controle_Demonstracao_Financeira).HasColumnName("Id_Controle_Demonstracao_Financeira");

            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");


            constuctor.HasOne(m => m.ControleDemonstracaoFinanceira).WithMany(m => m.ControleDemonstracaoFinanceiraControle)
                                .HasForeignKey(x => x.Id_Controle_Demonstracao_Financeira).HasPrincipalKey(x => x.Id_Controle_Demonstracao_Financeira); /* Join */
        }
    }
}
