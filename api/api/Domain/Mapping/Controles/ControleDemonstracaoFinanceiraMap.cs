﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleDemonstracaoFinanceiraMap : IEntityTypeConfiguration<ControleDemonstracaoFinanceira>
    {

        public void Configure(EntityTypeBuilder<ControleDemonstracaoFinanceira> constuctor)
        {

            constuctor.ToTable("controle_demonstracao_financeira");

            constuctor.Property(m => m.Id_Controle_Demonstracao_Financeira).HasColumnName("Id_Controle_Demonstracao_Financeira").IsRequired();
            constuctor.HasKey(o => o.Id_Controle_Demonstracao_Financeira);

            constuctor.Property(m => m.Controle_Demonstracao_Financeira_Nome).HasColumnName("Controle_Demonstracao_Financeira_Nome");
            constuctor.Property(m => m.Controle_Demonstracao_Financeira_Descricao).HasColumnName("Controle_Demonstracao_Financeira_Descricao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
        }
    }
}
