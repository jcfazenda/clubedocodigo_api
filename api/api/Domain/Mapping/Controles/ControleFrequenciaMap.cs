﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleFrequenciaMap : IEntityTypeConfiguration<ControleFrequencia>
    {

        public void Configure(EntityTypeBuilder<ControleFrequencia> constuctor)
        {

            constuctor.ToTable("controle_frequencia");

            constuctor.Property(m => m.Id_Controle_Frequencia).HasColumnName("Id_Controle_Frequencia").IsRequired();
            constuctor.HasKey(o => o.Id_Controle_Frequencia);

            constuctor.Property(m => m.Controle_Frequencia_Nome).HasColumnName("Controle_Frequencia_Nome");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
        }
    }
}
