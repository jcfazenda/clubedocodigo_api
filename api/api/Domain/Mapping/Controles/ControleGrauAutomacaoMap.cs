﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleGrauAutomacaoMap : IEntityTypeConfiguration<ControleGrauAutomacao>
    {

        public void Configure(EntityTypeBuilder<ControleGrauAutomacao> constuctor)
        {

            constuctor.ToTable("controle_grau_automacao");

            constuctor.Property(m => m.Id_Controle_Grau_Automacao).HasColumnName("Id_Controle_Grau_Automacao").IsRequired();
            constuctor.HasKey(o => o.Id_Controle_Grau_Automacao);

            constuctor.Property(m => m.Controle_Grau_Automacao_Nome).HasColumnName("Controle_Grau_Automacao_Nome");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
        }
    }
}
