﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleMap : IEntityTypeConfiguration<Controle>
    {

        public void Configure(EntityTypeBuilder<Controle> constuctor)
        {

            constuctor.ToTable("controle");

            constuctor.Property(m => m.Id_Controle).HasColumnName("Id_Controle").IsRequired();
            constuctor.HasKey(o => o.Id_Controle);

            constuctor.Property(m => m.Id_Controle_Categoria).HasColumnName("Id_Controle_Categoria");
            constuctor.Property(m => m.Id_Controle_Frequencia).HasColumnName("Id_Controle_Frequencia");
            constuctor.Property(m => m.Id_Controle_Grau_Automacao).HasColumnName("Id_Controle_Grau_Automacao");
            constuctor.Property(m => m.Id_Controle_Tipo).HasColumnName("Id_Controle_Tipo");

            constuctor.Property(m => m.Controle_Valor_Custo).HasColumnName("Controle_Valor_Custo");
            constuctor.Property(m => m.Controle_Nome).HasColumnName("Controle_Nome");
            constuctor.Property(m => m.Controle_Descricao).HasColumnName("Controle_Descricao"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.ControleCategoria).WithMany(m => m.Controle)
                                .HasForeignKey(x => x.Id_Controle_Categoria).HasPrincipalKey(x => x.Id_Controle_Categoria); /* Join */

        }
    }
}
