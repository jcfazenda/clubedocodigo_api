﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleObjetivoControleMap : IEntityTypeConfiguration<ControleObjetivoControle>
    {

        public void Configure(EntityTypeBuilder<ControleObjetivoControle> constuctor)
        {

            constuctor.ToTable("controle_objetivo_controle");

            constuctor.Property(m => m.Id_Controle_Objetivo_Controle).HasColumnName("Id_Controle_Objetivo_Controle").IsRequired();
            constuctor.HasKey(o => o.Id_Controle_Objetivo_Controle);

            constuctor.Property(m => m.Id_Controle).HasColumnName("Id_Controle");
            constuctor.Property(m => m.Id_Controle_Objetivo).HasColumnName("Id_Controle_Objetivo");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.ControleObjetivo).WithMany(m => m.ControleObjetivoControle)
                                .HasForeignKey(x => x.Id_Controle_Objetivo).HasPrincipalKey(x => x.Id_Controle_Objetivo); /* Join */

        }
    }
}
