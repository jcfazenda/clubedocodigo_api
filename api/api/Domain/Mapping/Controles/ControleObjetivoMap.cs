﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleObjetivoMap : IEntityTypeConfiguration<ControleObjetivo>
    {

        public void Configure(EntityTypeBuilder<ControleObjetivo> constuctor)
        {

            constuctor.ToTable("controle_objetivo");

            constuctor.Property(m => m.Id_Controle_Objetivo).HasColumnName("Id_Controle_Objetivo").IsRequired();
            constuctor.HasKey(o => o.Id_Controle_Objetivo);

            constuctor.Property(m => m.Controle_Objetivo_Nome).HasColumnName("Controle_Objetivo_Nome");
            constuctor.Property(m => m.Controle_Objetivo_Descricao).HasColumnName("Controle_Objetivo_Descricao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
        }
    }
}
