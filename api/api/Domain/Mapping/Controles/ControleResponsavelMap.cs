﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleResponsavelMap : IEntityTypeConfiguration<ControleResponsavel>
    {

        public void Configure(EntityTypeBuilder<ControleResponsavel> constuctor)
        {

            constuctor.ToTable("controle_responsavel");

            constuctor.Property(m => m.Id_Controle_Responsavel).HasColumnName("Id_Controle_Responsavel").IsRequired();
            constuctor.HasKey(o => o.Id_Controle_Responsavel);

            constuctor.Property(m => m.Id_Controle).HasColumnName("Id_Controle");
            constuctor.Property(m => m.Id_Usuario).HasColumnName("Id_Usuario");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.Usuarios).WithMany(m => m.ControleResponsavel)
                                .HasForeignKey(x => x.Id_Usuario).HasPrincipalKey(x => x.Id_Usuario); /* Join */

        }
    }
}
