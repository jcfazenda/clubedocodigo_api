﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleTesteMap : IEntityTypeConfiguration<ControleTeste>
    {

        public void Configure(EntityTypeBuilder<ControleTeste> constuctor)
        {

            constuctor.ToTable("controle_teste");

            constuctor.Property(m => m.Id_Controle_Teste).HasColumnName("Id_Controle_Teste").IsRequired();
            constuctor.HasKey(o => o.Id_Controle_Teste);

            constuctor.Property(m => m.Id_Controle).HasColumnName("Id_Controle");
            constuctor.Property(m => m.Id_Teste).HasColumnName("Id_Teste");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.Teste).WithMany(m => m.ControleTeste)
                                .HasForeignKey(x => x.Id_Teste).HasPrincipalKey(x => x.Id_Teste); /* Join */

        }
    }
}
