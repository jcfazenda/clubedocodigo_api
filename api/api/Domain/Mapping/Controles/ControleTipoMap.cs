﻿using api.Domain.Models.Controles;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Controles
{
    public sealed class ControleTipoMap : IEntityTypeConfiguration<ControleTipo>
    {

        public void Configure(EntityTypeBuilder<ControleTipo> constuctor)
        {

            constuctor.ToTable("controle_tipo");

            constuctor.Property(m => m.Id_Controle_Tipo).HasColumnName("Id_Controle_Tipo").IsRequired();
            constuctor.HasKey(o => o.Id_Controle_Tipo);

            constuctor.Property(m => m.Controle_Tipo_Nome).HasColumnName("Controle_Tipo_Nome");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
        }
    }
}
