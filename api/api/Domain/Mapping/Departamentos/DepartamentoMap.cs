﻿using api.Domain.Models.Departamentos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Departamentos
{
    public sealed class DepartamentoMap : IEntityTypeConfiguration<Departamento>
    {

        public void Configure(EntityTypeBuilder<Departamento> constuctor)
        {

            constuctor.ToTable("departamento");


            constuctor.Property(m => m.Id_Departamento).HasColumnName("Id_Departamento").IsRequired();
            constuctor.HasKey(o => o.Id_Departamento);

            constuctor.Property(m => m.Departamento_Nome).HasColumnName("Departamento_Nome");
            constuctor.Property(m => m.Departamento_Descricao).HasColumnName("Departamento_Descricao"); 

        }
    }
}

