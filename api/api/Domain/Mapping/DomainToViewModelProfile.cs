﻿using api.Domain.Models.Materia;
using api.Domain.Views.Output.Materia;
using AutoMapper;

namespace api.Domain.Mapping
{
    public class DomainToViewModelProfile : Profile
    {
        public DomainToViewModelProfile()
        {
            #region Materia

            CreateMap<Materias, MateriasOutput>()
                .ForMember(f => f.Id_Materia,    t => t.MapFrom(m => m.Id_Materia))
                .ForMember(f => f.Materia_Nome,  t => t.MapFrom(m => m.Materia_Nome))
                .ForMember(f => f.Fl_Ativo,      t => t.MapFrom(m => m.Fl_Ativo))
                ;

            #endregion

        }
    }
}
