﻿using api.Domain.Models.Grafics;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Grafics
{
    public sealed class DashboardMap : IEntityTypeConfiguration<Dashboard>
    {

        public void Configure(EntityTypeBuilder<Dashboard> constuctor)
        {

            constuctor.ToTable("dashboard");


            constuctor.Property(m => m.Id_Dashboard).HasColumnName("Id_Dashboard").IsRequired();
            constuctor.HasKey(o => o.Id_Dashboard);

            constuctor.Property(m => m.Id_Dashboard_Tipo).HasColumnName("Id_Dashboard_Tipo");
            constuctor.Property(m => m.Dashboard_Nome).HasColumnName("Dashboard_Nome");
            constuctor.Property(m => m.Dashboard_Descricao).HasColumnName("Dashboard_Descricao");
            constuctor.Property(m => m.Dashboard_Url).HasColumnName("Dashboard_Url");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
