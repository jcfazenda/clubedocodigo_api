﻿using api.Domain.Models.Grafics;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Grafics
{
    public sealed class DashboardUsuarioMap : IEntityTypeConfiguration<DashboardUsuario>
    {

        public void Configure(EntityTypeBuilder<DashboardUsuario> constuctor)
        {
            constuctor.ToTable("dashboard_usuario");

            constuctor.Property(m => m.Id_Dashboard_Usuario).HasColumnName("Id_Dashboard_Usuario").IsRequired();
            constuctor.HasKey(o => o.Id_Dashboard_Usuario);

            constuctor.Property(m => m.Id_Dashboard).HasColumnName("Id_Dashboard");
            constuctor.Property(m => m.Id_Usuario).HasColumnName("Id_Usuario"); 
            constuctor.Property(m => m.Fl_Checked).HasColumnName("Fl_Checked");
        }
    }
}
