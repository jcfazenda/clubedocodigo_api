﻿using api.Domain.Models.Incidentes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Incidentes
{
    public sealed class IncidenteCategoriaMap : IEntityTypeConfiguration<IncidenteCategoria>
    {

        public void Configure(EntityTypeBuilder<IncidenteCategoria> constuctor)
        {

            constuctor.ToTable("incidente_categoria");


            constuctor.Property(m => m.Id_Incidente_Categoria).HasColumnName("Id_Incidente_Categoria").IsRequired();
            constuctor.HasKey(o => o.Id_Incidente_Categoria);

            constuctor.Property(m => m.Incidente_Categoria_Nome).HasColumnName("Incidente_Categoria_Nome");
            constuctor.Property(m => m.Incidente_Categoria_Descricao).HasColumnName("Incidente_Categoria_Descricao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
