﻿using api.Domain.Models.Incidentes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Incidentes
{
    public sealed class IncidenteFluxoAprovacaoMap : IEntityTypeConfiguration<IncidenteFluxoAprovacao>
    {

        public void Configure(EntityTypeBuilder<IncidenteFluxoAprovacao> constuctor)
        {
            constuctor.ToTable("incidente_fluxo_aprovacao");

            constuctor.Property(m => m.Id_Incidente_Fluxo_Aprovacao).HasColumnName("Id_Incidente_Fluxo_Aprovacao").IsRequired();
            constuctor.HasKey(o => o.Id_Incidente_Fluxo_Aprovacao);

            constuctor.Property(m => m.Id_Workflow_Status).HasColumnName("Id_Workflow_Status");
            constuctor.Property(m => m.Id_Incidente).HasColumnName("Id_Incidente");
            constuctor.Property(m => m.Id_Risco_Avaliacao).HasColumnName("Id_Risco_Avaliacao");
            constuctor.Property(m => m.Id_Usuario).HasColumnName("Id_Usuario"); 
            constuctor.Property(m => m.Data_Evento).HasColumnName("Data_Evento");
            constuctor.Property(m => m.Workflow_Status_Observacao).HasColumnName("Workflow_Status_Observacao");

            constuctor.HasOne(m => m.WorkFlowStatus).WithMany(m => m.IncidenteFluxoAprovacao)
                      .HasForeignKey(x => x.Id_Workflow_Status).HasPrincipalKey(x => x.Id_Workflow_Status); /* Join */

            constuctor.HasOne(m => m.Usuarios).WithMany(m => m.IncidenteFluxoAprovacao)
                      .HasForeignKey(x => x.Id_Usuario).HasPrincipalKey(x => x.Id_Usuario); /* Join */

        }
    }
}
