﻿using api.Domain.Models.Incidentes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Incidentes
{
    public sealed class IncidenteMap : IEntityTypeConfiguration<Incidente>
    {

        public void Configure(EntityTypeBuilder<Incidente> constuctor)
        {

            constuctor.ToTable("incidente");


            constuctor.Property(m => m.Id_Incidente).HasColumnName("Id_Incidente").IsRequired();
            constuctor.HasKey(o => o.Id_Incidente);

            constuctor.Property(m => m.Id_Incidente_Categoria).HasColumnName("Id_Incidente_Categoria");
            constuctor.Property(m => m.Incidente_Nome).HasColumnName("Incidente_Nome");
            constuctor.Property(m => m.Incidente_Descricao).HasColumnName("Incidente_Descricao");

            constuctor.Property(m => m.Base_Origem).HasColumnName("Base_Origem");
            constuctor.Property(m => m.Perda_Financeira).HasColumnName("Perda_Financeira");
            constuctor.Property(m => m.Data_Ocorrencia).HasColumnName("Data_Ocorrencia");

            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
