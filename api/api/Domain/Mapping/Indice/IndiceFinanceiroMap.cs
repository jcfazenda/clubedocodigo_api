﻿using api.Domain.Models.Indice;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Indice
{
    public sealed class IncideFinanceiroMap : IEntityTypeConfiguration<IndiceFinanceiro>
    {

        public void Configure(EntityTypeBuilder<IndiceFinanceiro> constuctor)
        {

            constuctor.ToTable("indice_financeiro");


            constuctor.Property(m => m.Id_Indice_Financeiro).HasColumnName("Id_Indice_Financeiro").IsRequired();
            constuctor.HasKey(o => o.Id_Indice_Financeiro);
             
            constuctor.Property(m => m.Indice_Financeiro_Nome).HasColumnName("Indice_Financeiro_Nome");
            constuctor.Property(m => m.Indice_Financeiro_Descricao).HasColumnName("Indice_Financeiro_Descricao"); 
            constuctor.Property(m => m.Data_Ocorrencia).HasColumnName("Data_Ocorrencia");
            constuctor.Property(m => m.Valor_Referencia).HasColumnName("Valor_Referencia"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
