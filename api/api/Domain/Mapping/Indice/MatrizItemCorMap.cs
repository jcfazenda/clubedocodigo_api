﻿using api.Domain.Models.Indice;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Indice
{
    public sealed class MatrizItemCorMap : IEntityTypeConfiguration<MatrizItemCor>
    {

        public void Configure(EntityTypeBuilder<MatrizItemCor> constuctor)
        {

            constuctor.ToTable("matriz_item_cor");


            constuctor.Property(m => m.Id_Matriz_Item_Cor).HasColumnName("Id_Matriz_Item_Cor").IsRequired();
            constuctor.HasKey(o => o.Id_Matriz_Item_Cor);

            constuctor.Property(m => m.Id_Matriz_Item).HasColumnName("Id_Matriz_Item");
            constuctor.Property(m => m.Id_Matriz).HasColumnName("Id_Matriz");
            constuctor.Property(m => m.Background).HasColumnName("Background");
            constuctor.Property(m => m.Linha).HasColumnName("Linha");
            constuctor.Property(m => m.Coluna).HasColumnName("Coluna"); 
 
        }
    }
}
