﻿using api.Domain.Models.Indice;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Indice
{
    public sealed class MatrizItemMap : IEntityTypeConfiguration<MatrizItem>
    {

        public void Configure(EntityTypeBuilder<MatrizItem> constuctor)
        {

            constuctor.ToTable("matriz_item");


            constuctor.Property(m => m.Id_Matriz_Item).HasColumnName("Id_Matriz_Item").IsRequired();
            constuctor.HasKey(o => o.Id_Matriz_Item);

            constuctor.Property(m => m.Id_Matriz).HasColumnName("Id_Matriz");
            constuctor.Property(m => m.Matriz_Item_Nome).HasColumnName("Matriz_Item_Nome");
            constuctor.Property(m => m.Matriz_Item_Descricao).HasColumnName("Matriz_Item_Descricao");
            constuctor.Property(m => m.Tipo).HasColumnName("Tipo");
            constuctor.Property(m => m.Percentual).HasColumnName("Percentual");  
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");


            constuctor.HasOne(m => m.Matriz).WithMany(m => m.MatrizItem)
                                            .HasForeignKey(x => x.Id_Matriz).HasPrincipalKey(x => x.Id_Matriz); /* Join */

            //constuctor.HasOne(m => m.MatrizItemCor).WithMany(m => m.MatrizItem)
            //                    .HasForeignKey(x => x.Id_Matriz_Item).HasPrincipalKey(x => x.Id_Matriz_Item); /* Join */

        }
    }
}
