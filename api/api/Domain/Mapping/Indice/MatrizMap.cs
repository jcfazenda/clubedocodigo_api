﻿using api.Domain.Models.Indice;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Indice
{
    public sealed class MatrizMap : IEntityTypeConfiguration<Matriz>
    {

        public void Configure(EntityTypeBuilder<Matriz> constuctor)
        {

            constuctor.ToTable("matriz");


            constuctor.Property(m => m.Id_Matriz).HasColumnName("Id_Matriz").IsRequired();
            constuctor.HasKey(o => o.Id_Matriz);

            constuctor.Property(m => m.Matriz_Nome).HasColumnName("Matriz_Nome");
            constuctor.Property(m => m.Matriz_Descricao).HasColumnName("Matriz_Descricao"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo"); 

        }
    }
}
