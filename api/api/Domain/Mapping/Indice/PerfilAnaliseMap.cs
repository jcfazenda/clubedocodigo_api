﻿using api.Domain.Models.Indice;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Indice
{
    public sealed class PerfilAnaliseMap : IEntityTypeConfiguration<PerfilAnalise>
    {

        public void Configure(EntityTypeBuilder<PerfilAnalise> constuctor)
        {

            constuctor.ToTable("perfil_analise");


            constuctor.Property(m => m.Id_Perfil_Analise).HasColumnName("Id_Perfil_Analise").IsRequired();
            constuctor.HasKey(o => o.Id_Perfil_Analise);

            constuctor.Property(m => m.Id_Tipo_Avaliacao).HasColumnName("Id_Tipo_Avaliacao");
            constuctor.Property(m => m.Id_Matriz).HasColumnName("Id_Matriz");
            constuctor.Property(m => m.Id_Indice_Financeiro).HasColumnName("Id_Indice_Financeiro"); 

            constuctor.Property(m => m.Perfil_Analise_Nome).HasColumnName("Perfil_Analise_Nome");
            constuctor.Property(m => m.Perfil_Analise_Descricao).HasColumnName("Perfil_Analise_Descricao");
            constuctor.Property(m => m.Inerente_Residual_Planejado).HasColumnName("Inerente_Residual_Planejado");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.IndiceFinanceiro).WithMany(m => m.PerfilAnalise)
                                    .HasForeignKey(x => x.Id_Indice_Financeiro).HasPrincipalKey(x => x.Id_Indice_Financeiro); /* Join */

        }
    }
}
