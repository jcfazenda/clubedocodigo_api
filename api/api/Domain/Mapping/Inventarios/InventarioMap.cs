﻿using api.Domain.Models.Inventarios;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Inventarios
{
    public sealed class InventarioMap : IEntityTypeConfiguration<Inventario>
    {

        public void Configure(EntityTypeBuilder<Inventario> constuctor)
        {

            constuctor.ToTable("inventario");
            constuctor.Property(m => m.Id_Inventario).HasColumnName("Id_Inventario").IsRequired();
            constuctor.HasKey(o => o.Id_Inventario);

            constuctor.Property(m => m.Id_Inventario_Tipo).HasColumnName("Id_Inventario_Tipo");
            constuctor.Property(m => m.Inventario_Codigo).HasColumnName("Inventario_Codigo");
            constuctor.Property(m => m.Inventario_Nome).HasColumnName("Inventario_Nome");
            constuctor.Property(m => m.Inventario_Descricao).HasColumnName("Inventario_Descricao");
            constuctor.Property(m => m.Inventario_Anexo).HasColumnName("Inventario_Anexo");
            constuctor.Property(m => m.Inventario_Anexo_Nome).HasColumnName("Inventario_Anexo_Nome");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
        }
    }
}
