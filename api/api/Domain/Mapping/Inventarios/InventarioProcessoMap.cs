﻿using api.Domain.Models.Inventarios;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Inventarios
{
    public sealed class InventarioProcessoMap : IEntityTypeConfiguration<InventarioProcesso>
    {

        public void Configure(EntityTypeBuilder<InventarioProcesso> constuctor)
        {

            constuctor.ToTable("inventario_processo");
            constuctor.Property(m => m.Id_Inventario_Processo).HasColumnName("Id_Inventario_Processo").IsRequired();
            constuctor.HasKey(o => o.Id_Inventario_Processo);

            constuctor.Property(m => m.Id_Inventario).HasColumnName("Id_Inventario");
            constuctor.Property(m => m.Id_Processo).HasColumnName("Id_Processo");
            constuctor.Property(m => m.Anexo).HasColumnName("Anexo");

            constuctor.HasOne(m => m.Inventario).WithMany(m => m.InventarioProcesso)
                    .HasForeignKey(x => x.Id_Inventario).HasPrincipalKey(x => x.Id_Inventario); /* Join */

        }
    }
}