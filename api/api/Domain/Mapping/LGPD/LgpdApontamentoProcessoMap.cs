﻿using api.Domain.Models.LGPD;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.LGPD
{
    public sealed class LgpdApontamentoProcessoMap : IEntityTypeConfiguration<LgpdApontamentoProcesso>
    {

        public void Configure(EntityTypeBuilder<LgpdApontamentoProcesso> constuctor)
        {

            constuctor.ToTable("lgpd_apontamento_processo");
            constuctor.Property(m => m.Id_Lgpd_Apontamento_Processo).HasColumnName("Id_Lgpd_Apontamento_Processo").IsRequired();
            constuctor.HasKey(o => o.Id_Lgpd_Apontamento_Processo);

            constuctor.Property(m => m.Id_Apontamento).HasColumnName("Id_Apontamento");
            constuctor.Property(m => m.Id_Processo).HasColumnName("Id_Processo");


            constuctor.HasOne(m => m.Apontamento).WithMany(m => m.LgpdApontamentoProcesso)
                    .HasForeignKey(x => x.Id_Apontamento).HasPrincipalKey(x => x.Id_Apontamento); /* Join */

        }
    }
}
