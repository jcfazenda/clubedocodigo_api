﻿using api.Domain.Models.LGPD;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.LGPD
{
    public sealed class LgpdQuizMap : IEntityTypeConfiguration<LgpdQuiz>
    {

        public void Configure(EntityTypeBuilder<LgpdQuiz> constuctor)
        {

            constuctor.ToTable("lgpd_quiz");
            constuctor.Property(m => m.Id_Lgpd_Quiz).HasColumnName("Id_Lgpd_Quiz").IsRequired();
            constuctor.HasKey(o => o.Id_Lgpd_Quiz);

            constuctor.Property(m => m.Id_Lgpd_Tipo).HasColumnName("Id_Lgpd_Tipo");
            constuctor.Property(m => m.Id_Lgpd_Tipo_Dados).HasColumnName("Id_Lgpd_Tipo_Dados");
            constuctor.Property(m => m.Fl_Lgpd_Tipo_Dados_Coletado).HasColumnName("Fl_Lgpd_Tipo_Dados_Coletado");
            constuctor.Property(m => m.Lgpd_Quiz_Nome).HasColumnName("Lgpd_Quiz_Nome");
            constuctor.Property(m => m.Lgpd_Quiz_Descricao).HasColumnName("Lgpd_Quiz_Descricao");

            constuctor.HasOne(m => m.LgpdTipo).WithMany(m => m.LgpdQuiz)
                      .HasForeignKey(x => x.Id_Lgpd_Tipo).HasPrincipalKey(x => x.Id_Lgpd_Tipo); /* Join */


        }
    }
}
