﻿using api.Domain.Models.LGPD;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.LGPD
{
    public sealed class LgpdQuizProcessoMap : IEntityTypeConfiguration<LgpdQuizProcesso>
    {

        public void Configure(EntityTypeBuilder<LgpdQuizProcesso> constuctor)
        {

            constuctor.ToTable("lgpd_quiz_processo");
            constuctor.Property(m => m.Id_Lgpd_Quiz_Processo).HasColumnName("Id_Lgpd_Quiz_Processo").IsRequired();
            constuctor.HasKey(o => o.Id_Lgpd_Quiz_Processo);

            constuctor.Property(m => m.Id_Processo).HasColumnName("Id_Processo");
            constuctor.Property(m => m.Id_Lgpd_Quiz).HasColumnName("Id_Lgpd_Quiz");
            constuctor.Property(m => m.Lgpd_Quiz_Processo_Descricao).HasColumnName("Lgpd_Quiz_Processo_Descricao");
            constuctor.Property(m => m.Lgpd_Quiz_Processo_Descritivo).HasColumnName("Lgpd_Quiz_Processo_Descritivo");

            constuctor.HasOne(m => m.LgpdQuiz).WithMany(m => m.LgpdQuizProcesso)
                      .HasForeignKey(x => x.Id_Lgpd_Quiz).HasPrincipalKey(x => x.Id_Lgpd_Quiz); /* Join */


        }
    }
}
