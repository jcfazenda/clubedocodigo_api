﻿using api.Domain.Models.LGPD;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.LGPD
{
    public sealed class LgpdTipoDadosMap : IEntityTypeConfiguration<LgpdTipoDados>
    {

        public void Configure(EntityTypeBuilder<LgpdTipoDados> constuctor)
        {

            constuctor.ToTable("lgpd_tipo_dados");


            constuctor.Property(m => m.Id_Lgpd_Tipo_Dados).HasColumnName("Id_Lgpd_Tipo_Dados").IsRequired();
            constuctor.HasKey(o => o.Id_Lgpd_Tipo_Dados);

            constuctor.Property(m => m.Id_Lgpd_Tipo_Dados_Categoria).HasColumnName("Id_Lgpd_Tipo_Dados_Categoria");
            constuctor.Property(m => m.Lgpd_Tipo_Dados_Nome).HasColumnName("Lgpd_Tipo_Dados_Nome"); 


        }
    }
}
