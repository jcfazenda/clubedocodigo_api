﻿using api.Domain.Models.LGPD;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.LGPD
{
    public sealed class LgpdTipoMap : IEntityTypeConfiguration<LgpdTipo>
    {

        public void Configure(EntityTypeBuilder<LgpdTipo> constuctor)
        {

            constuctor.ToTable("lgpd_tipo");


            constuctor.Property(m => m.Id_Lgpd_Tipo).HasColumnName("Id_Lgpd_Tipo").IsRequired();
            constuctor.HasKey(o => o.Id_Lgpd_Tipo);

            constuctor.Property(m => m.Lgpd_Tipo_Nome).HasColumnName("Lgpd_Tipo_Nome"); 

        }
    }
}
