﻿
using api.Domain.Models.Materia;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Estados
{
    public sealed class MateriasMap : IEntityTypeConfiguration<Materias>
    {

        public void Configure(EntityTypeBuilder<Materias> constuctor)
        {

            constuctor.ToTable("materias"); /* nome da tabela no MySQL */

            constuctor.Property(m => m.Id_Materia).HasColumnName("Id_Materia").IsRequired();
            constuctor.HasKey(o => o.Id_Materia);

            constuctor.Property(m => m.Materia_Nome).HasColumnName("Materia_Nome");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
