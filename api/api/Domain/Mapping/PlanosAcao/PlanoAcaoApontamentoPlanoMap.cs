﻿using api.Domain.Models.PlanosAcao;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.PlanosAcao
{
    public sealed class PlanoAcaoApontamentoPlanoMap : IEntityTypeConfiguration<PlanoAcaoApontamentoPlano>
    {

        public void Configure(EntityTypeBuilder<PlanoAcaoApontamentoPlano> constuctor)
        {

            constuctor.ToTable("plano_acao_apontamento_plano");

            constuctor.Property(m => m.Id_Plano_Acao_Apontamento_Plano).HasColumnName("Id_Plano_Acao_Apontamento_Plano").IsRequired();
            constuctor.HasKey(o => o.Id_Plano_Acao_Apontamento_Plano);

            constuctor.Property(m => m.Id_Plano_Acao).HasColumnName("Id_Plano_Acao");
            constuctor.Property(m => m.Id_Apontamento).HasColumnName("Id_Apontamento"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.Apontamento).WithMany(m => m.PlanoAcaoApontamentoPlano)
                                  .HasForeignKey(x => x.Id_Apontamento).HasPrincipalKey(x => x.Id_Apontamento); /* Join */


        }
    }
}
