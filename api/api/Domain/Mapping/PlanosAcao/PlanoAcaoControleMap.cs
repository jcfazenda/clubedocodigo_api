﻿using api.Domain.Models.PlanosAcao;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.PlanosAcao
{
    public sealed class PlanoAcaoControleMap : IEntityTypeConfiguration<PlanoAcaoControle>
    {

        public void Configure(EntityTypeBuilder<PlanoAcaoControle> constuctor)
        {

            constuctor.ToTable("plano_acao_controle");

            constuctor.Property(m => m.Id_Plano_Acao_Controle).HasColumnName("Id_Plano_Acao_Controle").IsRequired();
            constuctor.HasKey(o => o.Id_Plano_Acao_Controle);

            constuctor.Property(m => m.Id_Controle).HasColumnName("Id_Controle");
            constuctor.Property(m => m.Id_Plano_Acao).HasColumnName("Id_Plano_Acao"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.Controle).WithMany(m => m.PlanoAcaoControle)
                      .HasForeignKey(x => x.Id_Controle).HasPrincipalKey(x => x.Id_Controle); /* Join */

        }
    }
}
