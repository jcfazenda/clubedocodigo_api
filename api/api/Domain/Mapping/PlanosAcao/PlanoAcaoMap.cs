﻿using api.Domain.Models.PlanosAcao;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.PlanosAcao
{
    public sealed class PlanoAcaoMap : IEntityTypeConfiguration<PlanoAcao>
    {

        public void Configure(EntityTypeBuilder<PlanoAcao> constuctor)
        {

            constuctor.ToTable("plano_acao");


            constuctor.Property(m => m.Id_Plano_Acao).HasColumnName("Id_Plano_Acao").IsRequired();
            constuctor.HasKey(o => o.Id_Plano_Acao);

            constuctor.Property(m => m.Id_Plano_Acao_Status).HasColumnName("Id_Plano_Acao_Status");
            constuctor.Property(m => m.Id_Departamento).HasColumnName("Id_Departamento");
            constuctor.Property(m => m.Plano_Acao_Nome).HasColumnName("Plano_Acao_Nome");
            constuctor.Property(m => m.Plano_Acao_Descricao).HasColumnName("Plano_Acao_Descricao");
            constuctor.Property(m => m.Plano_Acao_Descricao_Conclusao).HasColumnName("Plano_Acao_Descricao_Conclusao");

            constuctor.Property(m => m.Data_Criacao).HasColumnName("Data_Criacao");
            constuctor.Property(m => m.Data_Encerramento).HasColumnName("Data_Encerramento");

            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.Departamento).WithMany(m => m.PlanoAcao)
                        .HasForeignKey(x => x.Id_Departamento).HasPrincipalKey(x => x.Id_Departamento); /* Join */


        }
    }
}
