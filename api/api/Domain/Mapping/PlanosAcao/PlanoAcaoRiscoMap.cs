﻿using api.Domain.Models.PlanosAcao;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.PlanosAcao
{
    public sealed class PlanoAcaoRiscoMap : IEntityTypeConfiguration<PlanoAcaoRisco>
    {

        public void Configure(EntityTypeBuilder<PlanoAcaoRisco> constuctor)
        {

            constuctor.ToTable("plano_acao_risco");


            constuctor.Property(m => m.Id_Plano_Acao_Risco).HasColumnName("Id_Plano_Acao_Risco").IsRequired();
            constuctor.HasKey(o => o.Id_Plano_Acao_Risco);

            constuctor.Property(m => m.Id_Plano_Acao).HasColumnName("Id_Plano_Acao");
            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
