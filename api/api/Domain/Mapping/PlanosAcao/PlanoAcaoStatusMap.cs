﻿using api.Domain.Models.PlanosAcao;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.PlanosAcao
{
    public sealed class PlanoAcaoStatusMap : IEntityTypeConfiguration<PlanoAcaoStatus>
    {

        public void Configure(EntityTypeBuilder<PlanoAcaoStatus> constuctor)
        {

            constuctor.ToTable("plano_acao_status");


            constuctor.Property(m => m.Id_Plano_Acao_Status).HasColumnName("Id_Plano_Acao_Status").IsRequired();
            constuctor.HasKey(o => o.Id_Plano_Acao_Status);

            constuctor.Property(m => m.Plano_Acao_Status_Nome).HasColumnName("Plano_Acao_Status_Nome"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
