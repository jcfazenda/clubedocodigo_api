﻿using api.Domain.Models.PlanosAcao;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.PlanosAcao
{
    public sealed class PlanoAcaoStepMap : IEntityTypeConfiguration<PlanoAcaoStep>
    {

        public void Configure(EntityTypeBuilder<PlanoAcaoStep> constuctor)
        {

            constuctor.ToTable("plano_acao_step");

            constuctor.Property(m => m.Id_Plano_Acao_Step).HasColumnName("Id_Plano_Acao_Step").IsRequired();
            constuctor.HasKey(o => o.Id_Plano_Acao_Step);

            constuctor.Property(m => m.Id_Plano_Acao).HasColumnName("Id_Plano_Acao");
            constuctor.Property(m => m.Id_Step_Status).HasColumnName("Id_Step_Status");
            constuctor.Property(m => m.Id_Usuario_Responsavel).HasColumnName("Id_Usuario_Responsavel");


            constuctor.Property(m => m.Step_Nome).HasColumnName("Step_Nome");
            constuctor.Property(m => m.Step_Descricao).HasColumnName("Step_Descricao"); 
            constuctor.Property(m => m.Data_Inicio).HasColumnName("Data_Inicio");
            constuctor.Property(m => m.Data_Conclusao).HasColumnName("Data_Conclusao"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");


            constuctor.HasOne(m => m.Usuarios).WithMany(m => m.PlanoAcaoStep)
                    .HasForeignKey(x => x.Id_Usuario_Responsavel).HasPrincipalKey(x => x.Id_Usuario); /* Join */

            constuctor.HasOne(m => m.StepStatus).WithMany(m => m.PlanoAcaoStep)
                    .HasForeignKey(x => x.Id_Step_Status).HasPrincipalKey(x => x.Id_Step_Status); /* Join */
        }
    }
}
