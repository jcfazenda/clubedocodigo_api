﻿using api.Domain.Models.PlanosAcao;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.PlanosAcao
{
    public sealed class StepStatusMap : IEntityTypeConfiguration<StepStatus>
    {

        public void Configure(EntityTypeBuilder<StepStatus> constuctor)
        {

            constuctor.ToTable("step_status");


            constuctor.Property(m => m.Id_Step_Status).HasColumnName("Id_Step_Status").IsRequired();
            constuctor.HasKey(o => o.Id_Step_Status);

            constuctor.Property(m => m.Step_Status_Nome).HasColumnName("Step_Status_Nome");
            constuctor.Property(m => m.Icon).HasColumnName("Icon");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
