﻿using api.Domain.Models.Processos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Processos
{
    public sealed class LinhaNegocioMap : IEntityTypeConfiguration<LinhaNegocio>
    {

        public void Configure(EntityTypeBuilder<LinhaNegocio> constuctor)
        {

            constuctor.ToTable("linha_negocio");


            constuctor.Property(m => m.Id_Linha_Negocio).HasColumnName("Id_Linha_Negocio").IsRequired();
            constuctor.HasKey(o => o.Id_Linha_Negocio);
             
            constuctor.Property(m => m.Linha_Negocio_Nome).HasColumnName("Linha_Negocio_Nome");
            constuctor.Property(m => m.Linha_Negocio_Descricao).HasColumnName("Linha_Negocio_Descricao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
