﻿using api.Domain.Models.Processos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Processos
{
    public sealed class ProcessoAnexoMap : IEntityTypeConfiguration<ProcessoAnexo>
    {

        public void Configure(EntityTypeBuilder<ProcessoAnexo> constuctor)
        {

            constuctor.ToTable("processo_anexo");


            constuctor.Property(m => m.Id_Processo_Anexo).HasColumnName("Id_Processo_Anexo").IsRequired();
            constuctor.HasKey(o => o.Id_Processo_Anexo);

            constuctor.Property(m => m.Id_Processo).HasColumnName("Id_Processo");
            constuctor.Property(m => m.Processo_Anexo_Nome).HasColumnName("Processo_Anexo_Nome");
            constuctor.Property(m => m.Processo_Anexo_Byte).HasColumnName("Processo_Anexo_Byte"); 
        }
    }
}
