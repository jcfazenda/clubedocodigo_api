﻿using api.Domain.Models.Processos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Processos
{
    public sealed class ProcessoEmpresaMap : IEntityTypeConfiguration<ProcessoEmpresa>
    {

        public void Configure(EntityTypeBuilder<ProcessoEmpresa> constuctor)
        {

            constuctor.ToTable("processo_empresa");

            constuctor.Property(m => m.Id_Processo_Empresa).HasColumnName("Id_Processo_Empresa").IsRequired();
            constuctor.HasKey(o => o.Id_Processo_Empresa);

            constuctor.Property(m => m.Id_Processo).HasColumnName("Id_Processo");
            constuctor.Property(m => m.Id_Empresa).HasColumnName("Id_Empresa");

            constuctor.HasOne(m => m.Empresa).WithMany(m => m.ProcessoEmpresa)
                        .HasForeignKey(x => x.Id_Empresa).HasPrincipalKey(x => x.Id_Empresa); /* Join */

            constuctor.HasOne(m => m.Processo).WithMany(m => m.ProcessoEmpresa)
                      .HasForeignKey(x => x.Id_Processo).HasPrincipalKey(x => x.Id_Processo); /* Join */

        }
    }
}
