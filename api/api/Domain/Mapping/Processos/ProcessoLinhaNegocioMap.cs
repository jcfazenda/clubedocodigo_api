﻿using api.Domain.Models.Processos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Processos
{
    public sealed class ProcessoLinhaNegocioMap : IEntityTypeConfiguration<ProcessoLinhaNegocio>
    {

        public void Configure(EntityTypeBuilder<ProcessoLinhaNegocio> constuctor)
        {

            constuctor.ToTable("processo_linha_negocio");

            constuctor.Property(m => m.Id_Processo_Linha_Negocio).HasColumnName("Id_Processo_Linha_Negocio").IsRequired();
            constuctor.HasKey(o => o.Id_Processo_Linha_Negocio);

            constuctor.Property(m => m.Id_Processo).HasColumnName("Id_Processo");
            constuctor.Property(m => m.Id_Linha_Negocio).HasColumnName("Id_Linha_Negocio");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");  

        }
    }
}
