﻿using api.Domain.Models.Processos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Processos
{
    public sealed class ProcessoMap : IEntityTypeConfiguration<Processo>
    {

        public void Configure(EntityTypeBuilder<Processo> constuctor)
        {

            constuctor.ToTable("processo");


            constuctor.Property(m => m.Id_Processo).HasColumnName("Id_Processo").IsRequired();
            constuctor.HasKey(o => o.Id_Processo);
             
            constuctor.Property(m => m.Id_Processo_Nivel).HasColumnName("Id_Processo_Nivel");
            constuctor.Property(m => m.Processo_Nome).HasColumnName("Processo_Nome");
            constuctor.Property(m => m.Processo_Descricao).HasColumnName("Processo_Descricao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.ProcessoNivel).WithMany(m => m.Processo)
                        .HasForeignKey(x => x.Id_Processo_Nivel).HasPrincipalKey(x => x.Id_Processo_Nivel); /* Join */


        }
    }
}
