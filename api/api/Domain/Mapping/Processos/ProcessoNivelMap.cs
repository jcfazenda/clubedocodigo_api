﻿using api.Domain.Models.Processos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Processos
{
    public sealed class ProcessoNivelMap : IEntityTypeConfiguration<ProcessoNivel>
    {

        public void Configure(EntityTypeBuilder<ProcessoNivel> constuctor)
        {

            constuctor.ToTable("processo_nivel"); 

            constuctor.Property(m => m.Id_Processo_Nivel).HasColumnName("Id_Processo_Nivel").IsRequired();
            constuctor.HasKey(o => o.Id_Processo_Nivel);

            constuctor.Property(m => m.Processo_Nivel_Nome).HasColumnName("Processo_Nivel_Nome");
            constuctor.Property(m => m.Ordem).HasColumnName("Ordem");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
