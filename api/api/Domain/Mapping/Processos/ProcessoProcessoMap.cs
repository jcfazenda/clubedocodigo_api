﻿using api.Domain.Models.Processos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Processos
{
    public sealed class ProcessoProcessoMap : IEntityTypeConfiguration<ProcessoProcesso>
    {

        public void Configure(EntityTypeBuilder<ProcessoProcesso> constuctor)
        {

            constuctor.ToTable("processo_processo");
             
            constuctor.Property(m => m.Id_Processo_Processo).HasColumnName("Id_Processo_Processo").IsRequired();
            constuctor.HasKey(o => o.Id_Processo_Processo);

            constuctor.Property(m => m.Id_Processo).HasColumnName("Id_Processo");
            constuctor.Property(m => m.Id_Processo_Associado).HasColumnName("Id_Processo_Associado"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");


            constuctor.HasOne(m => m.Processos).WithMany(m => m.Processos)
                        .HasForeignKey(x => x.Id_Processo_Associado).HasPrincipalKey(x => x.Id_Processo); /* Join */


        }
    }
}
