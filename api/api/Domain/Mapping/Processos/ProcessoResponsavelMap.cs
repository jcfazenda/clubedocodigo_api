﻿using api.Domain.Models.Processos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Processos
{
    public sealed class ProcessoResponsavelMap : IEntityTypeConfiguration<ProcessoResponsavel>
    {

        public void Configure(EntityTypeBuilder<ProcessoResponsavel> constuctor)
        {

            constuctor.ToTable("processo_responsavel");

            constuctor.Property(m => m.Id_Processo_Responsavel).HasColumnName("Id_Processo_Responsavel").IsRequired();
            constuctor.HasKey(o => o.Id_Processo_Responsavel);

            constuctor.Property(m => m.Id_Processo).HasColumnName("Id_Processo");
            constuctor.Property(m => m.Id_Usuario).HasColumnName("Id_Usuario");

            constuctor.HasOne(m => m.Usuarios).WithMany(m => m.ProcessoResponsavel)
                                    .HasForeignKey(x => x.Id_Usuario).HasPrincipalKey(x => x.Id_Usuario); /* Join */


        }
    }
}
