﻿using api.Domain.Models.Processos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Processos
{
    public sealed class ProcessoRiscoMap : IEntityTypeConfiguration<ProcessoRisco>
    {

        public void Configure(EntityTypeBuilder<ProcessoRisco> constuctor)
        {

            constuctor.ToTable("processo_risco");

            constuctor.Property(m => m.Id_Processo_Risco).HasColumnName("Id_Processo_Risco").IsRequired();
            constuctor.HasKey(o => o.Id_Processo_Risco);

            constuctor.Property(m => m.Id_Processo).HasColumnName("Id_Processo"); 
            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");

            constuctor.HasOne(m => m.Riscos).WithMany(m => m.ProcessoRisco)
                                    .HasForeignKey(x => x.Id_Risco).HasPrincipalKey(x => x.Id_Risco); /* Join */


        }
    }
}
