﻿using api.Domain.Models.Processos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Processos
{
    public sealed class ProcessoUnidadeOrganizacionalMap : IEntityTypeConfiguration<ProcessoUnidadeOrganizacional>
    {

        public void Configure(EntityTypeBuilder<ProcessoUnidadeOrganizacional> constuctor)
        {

            constuctor.ToTable("processo_unidade_organizacional");


            constuctor.Property(m => m.Id_Processo_Unidade_Organizacional).HasColumnName("Id_Processo_Unidade_Organizacional").IsRequired();
            constuctor.HasKey(o => o.Id_Processo_Unidade_Organizacional);

            constuctor.Property(m => m.Id_Processo).HasColumnName("Id_Processo");
            constuctor.Property(m => m.Id_Unidade_Organizacional).HasColumnName("Id_Unidade_Organizacional"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.UnidadeOrganizacional).WithMany(m => m.ProcessoUnidadeOrganizacional)
                                    .HasForeignKey(x => x.Id_Unidade_Organizacional).HasPrincipalKey(x => x.Id_Unidade_Organizacional); /* Join */

        }
    }
}
