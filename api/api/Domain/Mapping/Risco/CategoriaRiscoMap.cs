﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class CategoriaRiscoMap : IEntityTypeConfiguration<CategoriaRisco>
    {

        public void Configure(EntityTypeBuilder<CategoriaRisco> constuctor)
        {

            constuctor.ToTable("categoria_risco");

            constuctor.Property(m => m.Id_Categoria_Risco).HasColumnName("Id_Categoria_Risco").IsRequired();
            constuctor.HasKey(o => o.Id_Categoria_Risco);

            constuctor.Property(m => m.Categoria_Risco_Nome).HasColumnName("Categoria_Risco_Nome"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
