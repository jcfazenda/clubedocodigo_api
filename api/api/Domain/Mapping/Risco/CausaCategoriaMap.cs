﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class CausaCategoriaMap : IEntityTypeConfiguration<CausaCategoria>
    {

        public void Configure(EntityTypeBuilder<CausaCategoria> constuctor)
        {

            constuctor.ToTable("causa_categoria");


            constuctor.Property(m => m.Id_Causa_Categoria).HasColumnName("Id_Causa_Categoria").IsRequired();
            constuctor.HasKey(o => o.Id_Causa_Categoria);
             
            constuctor.Property(m => m.Causa_Categoria_Nome).HasColumnName("Causa_Categoria_Nome");
            constuctor.Property(m => m.Causa_Categoria_Descricao).HasColumnName("Causa_Categoria_Descricao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
