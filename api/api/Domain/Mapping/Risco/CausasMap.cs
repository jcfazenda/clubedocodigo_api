﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class CausasMap : IEntityTypeConfiguration<Causas>
    {

        public void Configure(EntityTypeBuilder<Causas> constuctor)
        {

            constuctor.ToTable("causas");


            constuctor.Property(m => m.Id_Causa).HasColumnName("Id_Causa").IsRequired();
            constuctor.HasKey(o => o.Id_Causa);

            constuctor.Property(m => m.Id_Causa_Categoria).HasColumnName("Id_Causa_Categoria");
            constuctor.Property(m => m.Causa_Nome).HasColumnName("Causa_Nome");
            constuctor.Property(m => m.Causa_Descricao).HasColumnName("Causa_Descricao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
