﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class FatorRiscoMap : IEntityTypeConfiguration<FatorRisco>
    {

        public void Configure(EntityTypeBuilder<FatorRisco> constuctor)
        {

            constuctor.ToTable("fator_risco");


            constuctor.Property(m => m.Id_Fator_Risco).HasColumnName("Id_Fator_Risco").IsRequired();
            constuctor.HasKey(o => o.Id_Fator_Risco);

            constuctor.Property(m => m.Fator_Risco_Nome).HasColumnName("Fator_Risco_Nome");
            constuctor.Property(m => m.Fator_Risco_Descricao).HasColumnName("Fator_Risco_Descricao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
