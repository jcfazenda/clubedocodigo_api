﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class ImpactoMap : IEntityTypeConfiguration<Impacto>
    {

        public void Configure(EntityTypeBuilder<Impacto> constuctor)
        {

            constuctor.ToTable("impacto");

            constuctor.Property(m => m.Id_Impacto).HasColumnName("Id_Impacto").IsRequired();
            constuctor.HasKey(o => o.Id_Impacto);

            constuctor.Property(m => m.Impacto_Nome).HasColumnName("Impacto_Nome");
            constuctor.Property(m => m.Impacto_Descricao).HasColumnName("Impacto_Descricao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo"); 

        }
    }
}
