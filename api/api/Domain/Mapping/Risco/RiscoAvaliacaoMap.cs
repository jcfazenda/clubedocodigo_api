﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoAvaliacaoMap : IEntityTypeConfiguration<RiscoAvaliacao>
    {

        public void Configure(EntityTypeBuilder<RiscoAvaliacao> constuctor)
        {

            constuctor.ToTable("risco_avaliacao");


            constuctor.Property(m => m.Id_Risco_Avaliacao).HasColumnName("Id_Risco_Avaliacao").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Avaliacao);

            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");
            constuctor.Property(m => m.Id_Risco_Avaliacao_Status).HasColumnName("Id_Risco_Avaliacao_Status");
            constuctor.Property(m => m.Id_Risco_Tratamento_Tipo).HasColumnName("Id_Risco_Tratamento_Tipo");
            constuctor.Property(m => m.Id_Usuario).HasColumnName("Id_Usuario");

            constuctor.Property(m => m.Data_Inicio_Avaliacao).HasColumnName("Data_Inicio_Avaliacao");
            constuctor.Property(m => m.Data_Encerramento_Avaliacao).HasColumnName("Data_Encerramento_Avaliacao");

            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
            constuctor.Property(m => m.Fl_Excluido).HasColumnName("Fl_Excluido");

            constuctor.HasOne(m => m.Riscos).WithMany(m => m.RiscoAvaliacao)
                                            .HasForeignKey(x => x.Id_Risco).HasPrincipalKey(x => x.Id_Risco); /* Join */

            constuctor.HasOne(m => m.RiscoAvaliacaoStatus).WithMany(m => m.RiscoAvaliacao)
                                            .HasForeignKey(x => x.Id_Risco_Avaliacao_Status).HasPrincipalKey(x => x.Id_Risco_Avaliacao_Status); /* Join */

            constuctor.HasOne(m => m.Usuarios).WithMany(m => m.RiscoAvaliacao)
                                            .HasForeignKey(x => x.Id_Usuario).HasPrincipalKey(x => x.Id_Usuario); /* Join */

        }
    }
}
