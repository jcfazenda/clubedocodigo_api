﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoAvaliacaoMatrizMap : IEntityTypeConfiguration<RiscoAvaliacaoMatriz>
    {

        public void Configure(EntityTypeBuilder<RiscoAvaliacaoMatriz> constuctor)
        {

            constuctor.ToTable("risco_avaliacao_matriz");


            constuctor.Property(m => m.Id_Risco_Avaliacao_Matriz).HasColumnName("Id_Risco_Avaliacao_Matriz").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Avaliacao_Matriz);

            constuctor.Property(m => m.Id_Risco_Avaliacao).HasColumnName("Id_Risco_Avaliacao");
            constuctor.Property(m => m.Id_Matriz_Item).HasColumnName("Id_Matriz_Item");
            constuctor.Property(m => m.Inerente_Residual_Planejado).HasColumnName("Inerente_Residual_Planejado"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
            constuctor.Property(m => m.Fl_Pin).HasColumnName("Fl_Pin");


        }
    }
}
