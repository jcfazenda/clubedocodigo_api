﻿ 
using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoAvaliacaoStatusMap : IEntityTypeConfiguration<RiscoAvaliacaoStatus>
    {

        public void Configure(EntityTypeBuilder<RiscoAvaliacaoStatus> constuctor)
        {

            constuctor.ToTable("risco_avaliacao_status");

            constuctor.Property(m => m.Id_Risco_Avaliacao_Status).HasColumnName("Id_Risco_Avaliacao_Status").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Avaliacao_Status);

            constuctor.Property(m => m.Avaliacao_Status_Nome).HasColumnName("Avaliacao_Status_Nome");
            constuctor.Property(m => m.Panel_Color).HasColumnName("Panel_Color");
            constuctor.Property(m => m.Label_Color).HasColumnName("Label_Color");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
