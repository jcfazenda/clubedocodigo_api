﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoAvaliacaoWorkflowAnexoMap : IEntityTypeConfiguration<RiscoAvaliacaoWorkflowAnexo>
    {

        public void Configure(EntityTypeBuilder<RiscoAvaliacaoWorkflowAnexo> constuctor)
        {

            constuctor.ToTable("risco_avaliacao_workflow_anexo");


            constuctor.Property(m => m.Id_Risco_Avaliacao_Workflow_Anexo).HasColumnName("Id_Risco_Avaliacao_Workflow_Anexo").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Avaliacao_Workflow_Anexo);

            constuctor.Property(m => m.Id_Risco_Avaliacao_Workflow).HasColumnName("Id_Risco_Avaliacao_Workflow"); 

            constuctor.Property(m => m.Risco_Avaliacao_Workflow_Anexo_Nome).HasColumnName("Risco_Avaliacao_Workflow_Anexo_Nome");
            constuctor.Property(m => m.Risco_Avaliacao_Workflow_Anexo_Byte).HasColumnName("Risco_Avaliacao_Workflow_Anexo_Byte"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo"); 

        }
    }
}
