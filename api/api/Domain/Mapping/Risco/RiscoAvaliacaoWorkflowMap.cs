﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoAvaliacaoWorkflowMap : IEntityTypeConfiguration<RiscoAvaliacaoWorkflow>
    {

        public void Configure(EntityTypeBuilder<RiscoAvaliacaoWorkflow> constuctor)
        {

            constuctor.ToTable("risco_avaliacao_workflow");


            constuctor.Property(m => m.Id_Risco_Avaliacao_Workflow).HasColumnName("Id_Risco_Avaliacao_Workflow").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Avaliacao_Workflow);

            constuctor.Property(m => m.Id_Risco_Avaliacao_Workflow_Resposta).HasColumnName("Id_Risco_Avaliacao_Workflow_Resposta");
            constuctor.Property(m => m.Id_Risco_Avaliacao).HasColumnName("Id_Risco_Avaliacao");
            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");
            constuctor.Property(m => m.Id_Risco_Avaliacao_Status).HasColumnName("Id_Risco_Avaliacao_Status");
            constuctor.Property(m => m.Id_Usuario).HasColumnName("Id_Usuario");

            constuctor.Property(m => m.Risco_Avaliacao_Workflow_Nome).HasColumnName("Risco_Avaliacao_Workflow_Nome");
            constuctor.Property(m => m.Risco_Avaliacao_Workflow_Descricao).HasColumnName("Risco_Avaliacao_Workflow_Descricao");
            constuctor.Property(m => m.Data_Evento).HasColumnName("Data_Evento"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");


            constuctor.HasOne(m => m.Usuario).WithMany(m => m.RiscoAvaliacaoWorkflow)
                                .HasForeignKey(x => x.Id_Usuario).HasPrincipalKey(x => x.Id_Usuario); /* Join */

            constuctor.HasOne(m => m.RiscoAvaliacaoStatus).WithMany(m => m.RiscoAvaliacaoWorkflow)
                    .HasForeignKey(x => x.Id_Risco_Avaliacao_Status).HasPrincipalKey(x => x.Id_Risco_Avaliacao_Status); /* Join */ 
 

        }
    }
}
