﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoCategoriaMap : IEntityTypeConfiguration<RiscoCategoria>
    {

        public void Configure(EntityTypeBuilder<RiscoCategoria> constuctor)
        {

            constuctor.ToTable("risco_categoria");


            constuctor.Property(m => m.Id_Risco_Categoria).HasColumnName("Id_Risco_Categoria").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Categoria);

            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");
            constuctor.Property(m => m.Id_Categoria_Risco).HasColumnName("Id_Categoria_Risco"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.CategoriaRisco).WithMany(m => m.RiscoCategoria)
                    .HasForeignKey(x => x.Id_Categoria_Risco).HasPrincipalKey(x => x.Id_Categoria_Risco); /* Join */

        }
    }
}
