﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoCausaMap : IEntityTypeConfiguration<RiscoCausa>
    {

        public void Configure(EntityTypeBuilder<RiscoCausa> constuctor)
        {

            constuctor.ToTable("risco_causa");

            constuctor.Property(m => m.Id_Risco_Causa).HasColumnName("Id_Risco_Causa").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Causa);

            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");
            constuctor.Property(m => m.Id_Causa).HasColumnName("Id_Causa");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
            constuctor.Property(m => m.Id_Matriz_Item).HasColumnName("Id_Matriz_Item");

            constuctor.HasOne(m => m.MatrizItem).WithMany(m => m.RiscoCausa)
                                .HasForeignKey(x => x.Id_Matriz_Item).HasPrincipalKey(x => x.Id_Matriz_Item); /* Join */

            constuctor.HasOne(m => m.Causas).WithMany(m => m.RiscoCausa)
                                .HasForeignKey(x => x.Id_Causa).HasPrincipalKey(x => x.Id_Causa); /* Join */

        }
    }
}
