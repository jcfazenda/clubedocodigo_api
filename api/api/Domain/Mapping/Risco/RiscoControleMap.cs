﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoControleMap : IEntityTypeConfiguration<RiscoControle>
    {

        public void Configure(EntityTypeBuilder<RiscoControle> constuctor)
        {

            constuctor.ToTable("risco_controle"); 

            constuctor.Property(m => m.Id_Risco_Controle).HasColumnName("Id_Risco_Controle").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Controle);

            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");
            constuctor.Property(m => m.Id_Controle).HasColumnName("Id_Controle"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.Controle).WithMany(m => m.RiscoControle)
                                            .HasForeignKey(x => x.Id_Controle).HasPrincipalKey(x => x.Id_Controle); /* Join */

        }
    }
}
