﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoFatorMap : IEntityTypeConfiguration<RiscoFator>
    {

        public void Configure(EntityTypeBuilder<RiscoFator> constuctor)
        {

            constuctor.ToTable("risco_fator");

            constuctor.Property(m => m.Id_Risco_Fator).HasColumnName("Id_Risco_Fator").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Fator);

            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");
            constuctor.Property(m => m.Id_Fator_Risco).HasColumnName("Id_Fator_Risco");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.FatorRisco).WithMany(m => m.RiscoFator)
                                            .HasForeignKey(x => x.Id_Fator_Risco).HasPrincipalKey(x => x.Id_Fator_Risco); /* Join */

        }
    }
}
