﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoImpactoMap : IEntityTypeConfiguration<RiscoImpacto>
    {

        public void Configure(EntityTypeBuilder<RiscoImpacto> constuctor)
        {
            constuctor.ToTable("risco_impacto");

            constuctor.Property(m => m.Id_Risco_Impacto).HasColumnName("Id_Risco_Impacto").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Impacto);

            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");
            constuctor.Property(m => m.Id_Impacto).HasColumnName("Id_Impacto");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
            constuctor.Property(m => m.Id_Matriz_Item).HasColumnName("Id_Matriz_Item");

            constuctor.HasOne(m => m.MatrizItem).WithMany(m => m.RiscoImpacto)
                                .HasForeignKey(x => x.Id_Matriz_Item).HasPrincipalKey(x => x.Id_Matriz_Item); /* Join */

            constuctor.HasOne(m => m.Impacto).WithMany(m => m.RiscoImpacto)
                                            .HasForeignKey(x => x.Id_Impacto).HasPrincipalKey(x => x.Id_Impacto); /* Join */

        }
    }
}
