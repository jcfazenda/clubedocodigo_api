﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoIncidenteMap : IEntityTypeConfiguration<RiscoIncidente>
    {

        public void Configure(EntityTypeBuilder<RiscoIncidente> constuctor)
        {

            constuctor.ToTable("risco_incidente");

            constuctor.Property(m => m.Id_Risco_Incidente).HasColumnName("Id_Risco_Incidente").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Incidente);

            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");
            constuctor.Property(m => m.Id_Incidente).HasColumnName("Id_Incidente");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.Incidente).WithMany(m => m.RiscoIncidente)
                                .HasForeignKey(x => x.Id_Incidente).HasPrincipalKey(x => x.Id_Incidente); /* Join */

            constuctor.HasOne(m => m.Riscos).WithMany(m => m.RiscoIncidente)
                    .HasForeignKey(x => x.Id_Risco).HasPrincipalKey(x => x.Id_Risco); /* Join */

        }
    }
}
