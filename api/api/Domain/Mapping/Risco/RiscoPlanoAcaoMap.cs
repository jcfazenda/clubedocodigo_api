﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoPlanoAcaoMap : IEntityTypeConfiguration<RiscoPlanoAcao>
    {

        public void Configure(EntityTypeBuilder<RiscoPlanoAcao> constuctor)
        {

            constuctor.ToTable("risco_plano_acao");

            constuctor.Property(m => m.Id_Risco_Plano_Acao).HasColumnName("Id_Risco_Plano_Acao").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Plano_Acao);

            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");
            constuctor.Property(m => m.Id_Plano_Acao).HasColumnName("Id_Plano_Acao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.PlanoAcao).WithMany(m => m.RiscoPlanoAcao)
                                .HasForeignKey(x => x.Id_Plano_Acao).HasPrincipalKey(x => x.Id_Plano_Acao); /* Join */

        }
    }
}
