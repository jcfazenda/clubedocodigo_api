﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoProcessoMap : IEntityTypeConfiguration<RiscoProcesso>
    {

        public void Configure(EntityTypeBuilder<RiscoProcesso> constuctor)
        {

            constuctor.ToTable("risco_processo");

            constuctor.Property(m => m.Id_Risco_Processo).HasColumnName("Id_Risco_Processo").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Processo);

            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");
            constuctor.Property(m => m.Id_Processo).HasColumnName("Id_Processo");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.Processo).WithMany(m => m.RiscoProcesso)
                                .HasForeignKey(x => x.Id_Processo).HasPrincipalKey(x => x.Id_Processo); /* Join */

        }
    }
}
