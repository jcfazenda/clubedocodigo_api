﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoResponsavelMap : IEntityTypeConfiguration<RiscoResponsavel>
    {

        public void Configure(EntityTypeBuilder<RiscoResponsavel> constuctor)
        {

            constuctor.ToTable("risco_responsavel");

            constuctor.Property(m => m.Id_Risco_Responsavel).HasColumnName("Id_Risco_Responsavel").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Responsavel);

            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");
            constuctor.Property(m => m.Id_Usuario).HasColumnName("Id_Usuario");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.Usuarios).WithMany(m => m.RiscoResponsavel)
                                .HasForeignKey(x => x.Id_Usuario).HasPrincipalKey(x => x.Id_Usuario); /* Join */

        }
    }
}
