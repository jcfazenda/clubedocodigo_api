﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoRiscoMap : IEntityTypeConfiguration<RiscoRisco>
    {

        public void Configure(EntityTypeBuilder<RiscoRisco> constuctor)
        {

            constuctor.ToTable("risco_risco");

            constuctor.Property(m => m.Id_Risco_Risco).HasColumnName("Id_Risco_Risco").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Risco);

            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco");
            constuctor.Property(m => m.Id_Risco_Relacionado).HasColumnName("Id_Risco_Relacionado");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.Riscos).WithMany(m => m.RiscoRisco)
                                .HasForeignKey(x => x.Id_Risco_Relacionado).HasPrincipalKey(x => x.Id_Risco); /* Join */

        }
    }
}
