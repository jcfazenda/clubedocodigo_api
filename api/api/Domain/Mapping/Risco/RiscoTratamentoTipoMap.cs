﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscoTratamentoTipoMap : IEntityTypeConfiguration<RiscoTratamentoTipo>
    {

        public void Configure(EntityTypeBuilder<RiscoTratamentoTipo> constuctor)
        {

            constuctor.ToTable("risco_tratamento_tipo"); 

            constuctor.Property(m => m.Id_Risco_Tratamento_Tipo).HasColumnName("Id_Risco_Tratamento_Tipo").IsRequired();
            constuctor.HasKey(o => o.Id_Risco_Tratamento_Tipo);

            constuctor.Property(m => m.Tratamento_Tipo_Nome).HasColumnName("Tratamento_Tipo_Nome"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
