﻿using api.Domain.Models.Risco;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Risco
{
    public sealed class RiscosMap : IEntityTypeConfiguration<Riscos>
    {

        public void Configure(EntityTypeBuilder<Riscos> constuctor)
        {

            constuctor.ToTable("risco");

            constuctor.Property(m => m.Id_Risco).HasColumnName("Id_Risco").IsRequired();
            constuctor.HasKey(o => o.Id_Risco);

            constuctor.Property(m => m.Id_Risco_Tratamento_Tipo).HasColumnName("Id_Risco_Tratamento_Tipo");
            constuctor.Property(m => m.Id_Perfil_Analise).HasColumnName("Id_Perfil_Analise");
            constuctor.Property(m => m.Id_Usuario).HasColumnName("Id_Usuario");
            constuctor.Property(m => m.Risco_Nome).HasColumnName("Risco_Nome");
            constuctor.Property(m => m.Risco_Descricao).HasColumnName("Risco_Descricao");
            constuctor.Property(m => m.Risco_Tratamento_Tipo_Descricao).HasColumnName("Risco_Tratamento_Tipo_Descricao");
            constuctor.Property(m => m.Fl_Exclusivo).HasColumnName("Fl_Exclusivo");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
            constuctor.Property(m => m.Fl_Excluido).HasColumnName("Fl_Excluido");

        }
    }
}
