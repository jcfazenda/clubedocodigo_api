﻿using api.Domain.Models.Templates.Emails;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Templates.Emails
{
    public sealed class TemplateEmailMap : IEntityTypeConfiguration<TemplateEmail>
    {

        public void Configure(EntityTypeBuilder<TemplateEmail> constuctor)
        {

            constuctor.ToTable("template_email");


            constuctor.Property(m => m.Id_Template_Email).HasColumnName("Id_Template_Email").IsRequired();
            constuctor.HasKey(o => o.Id_Template_Email);

            constuctor.Property(m => m.Id_Tela).HasColumnName("Id_Tela");
            constuctor.Property(m => m.Id_Tela_Funcao).HasColumnName("Id_Tela_Funcao"); 

            constuctor.Property(m => m.Evento).HasColumnName("Evento");
            constuctor.Property(m => m.Texto).HasColumnName("Texto");
            constuctor.Property(m => m.Link_Button).HasColumnName("Link_Button"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.Tela).WithMany(m => m.TemplateEmail)
                      .HasForeignKey(x => x.Id_Tela).HasPrincipalKey(x => x.Id_Tela); /* Join */

            constuctor.HasOne(m => m.TelaFuncao).WithMany(m => m.TemplateEmail)
                        .HasForeignKey(x => x.Id_Tela_Funcao).HasPrincipalKey(x => x.Id_Tela_Funcao); /* Join */


        }
    }
}
