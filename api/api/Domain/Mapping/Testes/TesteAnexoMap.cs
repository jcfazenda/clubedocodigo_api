﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteAnexoMap : IEntityTypeConfiguration<TesteAnexo>
    {

        public void Configure(EntityTypeBuilder<TesteAnexo> constuctor)
        {

            constuctor.ToTable("teste_anexo");

            constuctor.Property(m => m.Id_Teste_Anexo).HasColumnName("Id_Teste_Anexo").IsRequired();
            constuctor.HasKey(o => o.Id_Teste_Anexo);

            constuctor.Property(m => m.Id_Teste).HasColumnName("Id_Teste");
            constuctor.Property(m => m.Teste_Anexo_Nome).HasColumnName("Teste_Anexo_Nome");
            constuctor.Property(m => m.Teste_Anexo_Byte).HasColumnName("Teste_Anexo_Byte"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo"); 

        }
    }
}
