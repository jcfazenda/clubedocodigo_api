﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteApontamentoTesteMap : IEntityTypeConfiguration<TesteApontamentoTeste>
    {

        public void Configure(EntityTypeBuilder<TesteApontamentoTeste> constuctor)
        {

            constuctor.ToTable("teste_apontamento_teste");

            constuctor.Property(m => m.Id_Teste_Apontamento_Teste).HasColumnName("Id_Teste_Apontamento_Teste").IsRequired();
            constuctor.HasKey(o => o.Id_Teste_Apontamento_Teste);

            constuctor.Property(m => m.Id_Apontamento).HasColumnName("Id_Apontamento");
            constuctor.Property(m => m.Id_Teste).HasColumnName("Id_Teste");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo"); 

            constuctor.HasOne(m => m.Apontamento).WithMany(m => m.TesteApontamentoTeste)
                    .HasForeignKey(x => x.Id_Apontamento).HasPrincipalKey(x => x.Id_Apontamento); /* Join */

        }
             
    }
}

