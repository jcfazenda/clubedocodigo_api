﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteFluxoAprovacaoMap : IEntityTypeConfiguration<TesteFluxoAprovacao>
    {

        public void Configure(EntityTypeBuilder<TesteFluxoAprovacao> constuctor)
        {

            constuctor.ToTable("teste_fluxo_aprovacao");

            constuctor.Property(m => m.Id_Teste_Fluxo_Aprovacao).HasColumnName("Id_Teste_Fluxo_Aprovacao").IsRequired();
            constuctor.HasKey(o => o.Id_Teste_Fluxo_Aprovacao);

            constuctor.Property(m => m.Id_Workflow_Status).HasColumnName("Id_Workflow_Status");
            constuctor.Property(m => m.Id_Teste).HasColumnName("Id_Teste");
            constuctor.Property(m => m.Id_Usuario).HasColumnName("Id_Usuario"); 
            constuctor.Property(m => m.Data_Evento).HasColumnName("Data_Evento");
            constuctor.Property(m => m.Workflow_Status_Observacao).HasColumnName("Workflow_Status_Observacao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.WorkFlowStatus).WithMany(m => m.TesteFluxoAprovacao)
                      .HasForeignKey(x => x.Id_Workflow_Status).HasPrincipalKey(x => x.Id_Workflow_Status); /* Join */

            constuctor.HasOne(m => m.Usuarios).WithMany(m => m.TesteFluxoAprovacao)
                      .HasForeignKey(x => x.Id_Usuario).HasPrincipalKey(x => x.Id_Usuario); /* Join */

        }
    }
}
