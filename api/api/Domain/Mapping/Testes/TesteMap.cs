﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteMap : IEntityTypeConfiguration<Teste>
    {

        public void Configure(EntityTypeBuilder<Teste> constuctor)
        {

            constuctor.ToTable("teste");

            constuctor.Property(m => m.Id_Teste).HasColumnName("Id_Teste").IsRequired();
            constuctor.HasKey(o => o.Id_Teste);

            constuctor.Property(m => m.Id_Teste_Status).HasColumnName("Id_Teste_Status");
            constuctor.Property(m => m.Teste_Nome).HasColumnName("Teste_Nome");
            constuctor.Property(m => m.Teste_Descricao).HasColumnName("Teste_Descricao"); 
            constuctor.Property(m => m.Fl_Resultado_Efetivo).HasColumnName("Fl_Resultado_Efetivo");
            constuctor.Property(m => m.Fl_Exclusivo).HasColumnName("Fl_Exclusivo");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.TesteStatus).WithMany(m => m.Teste)
                      .HasForeignKey(x => x.Id_Teste_Status).HasPrincipalKey(x => x.Id_Teste_Status); /* Join */
             
        }
    }
}
