﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteProcedimentoAmostraAnexoMap : IEntityTypeConfiguration<TesteProcedimentoAmostraAnexo>
    {

        public void Configure(EntityTypeBuilder<TesteProcedimentoAmostraAnexo> constuctor)
        {

            constuctor.ToTable("teste_procedimento_amostra_anexo");

            constuctor.Property(m => m.Id_Teste_Procedimento_Amostra_Anexo).HasColumnName("Id_Teste_Procedimento_Amostra_Anexo").IsRequired();
            constuctor.HasKey(o => o.Id_Teste_Procedimento_Amostra_Anexo);

            constuctor.Property(m => m.Id_Teste_Procedimento).HasColumnName("Id_Teste_Procedimento");

            constuctor.Property(m => m.Teste_Procedimento_Amostra_Anexo_Nome).HasColumnName("Teste_Procedimento_Amostra_Anexo_Nome");
            constuctor.Property(m => m.Teste_Procedimento_Amostra_Anexo_Byte).HasColumnName("Teste_Procedimento_Amostra_Anexo_Byte");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
