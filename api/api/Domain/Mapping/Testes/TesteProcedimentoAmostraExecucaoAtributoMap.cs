﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteProcedimentoAmostraExecucaoAtributoMap : IEntityTypeConfiguration<TesteProcedimentoAmostraExecucaoAtributo>
    {

        public void Configure(EntityTypeBuilder<TesteProcedimentoAmostraExecucaoAtributo> constuctor)
        {

            constuctor.ToTable("teste_procedimento_amostra_execucao_atributo");

            constuctor.Property(m => m.Id_Teste_Procedimento_Amostra_Execucao_Atributo).HasColumnName("Id_Teste_Procedimento_Amostra_Execucao_Atributo").IsRequired();
            constuctor.HasKey(o => o.Id_Teste_Procedimento_Amostra_Execucao_Atributo);

            constuctor.Property(m => m.Id_Teste_Procedimento_Amostra_Execucao).HasColumnName("Id_Teste_Procedimento_Amostra_Execucao");
            constuctor.Property(m => m.Id_Teste_Procedimento_Amostra).HasColumnName("Id_Teste_Procedimento_Amostra");
            constuctor.Property(m => m.Teste_Procedimento_Amostra_Execucao_Atributo_Nome).HasColumnName("Teste_Procedimento_Amostra_Execucao_Atributo_Nome");
            constuctor.Property(m => m.Fl_Executado).HasColumnName("Fl_Executado");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
             

        }
    }
}
