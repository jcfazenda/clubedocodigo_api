﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteProcedimentoAmostraExecucaoMap : IEntityTypeConfiguration<TesteProcedimentoAmostraExecucao>
    {

        public void Configure(EntityTypeBuilder<TesteProcedimentoAmostraExecucao> constuctor)
        {

            constuctor.ToTable("teste_procedimento_amostra_execucao");

            constuctor.Property(m => m.Id_Teste_Procedimento_Amostra_Execucao).HasColumnName("Id_Teste_Procedimento_Amostra_Execucao").IsRequired();
            constuctor.HasKey(o => o.Id_Teste_Procedimento_Amostra_Execucao); 
            constuctor.Property(m => m.Id_Teste_Procedimento_Amostra).HasColumnName("Id_Teste_Procedimento_Amostra"); 
            constuctor.Property(m => m.Teste_Procedimento_Amostra_Execucao_Nome).HasColumnName("Teste_Procedimento_Amostra_Execucao_Nome");
            constuctor.Property(m => m.Fl_Executado).HasColumnName("Fl_Executado");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.TesteProcedimentoAmostra).WithMany(m => m.TesteProcedimentoAmostraExecucao)
                      .HasForeignKey(x => x.Id_Teste_Procedimento_Amostra).HasPrincipalKey(x => x.Id_Teste_Procedimento_Amostra); /* Join */

        }
    }
}
