﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteProcedimentoAmostraMap : IEntityTypeConfiguration<TesteProcedimentoAmostra>
    {

        public void Configure(EntityTypeBuilder<TesteProcedimentoAmostra> constuctor)
        {

            constuctor.ToTable("teste_procedimento_amostra");

            constuctor.Property(m => m.Id_Teste_Procedimento_Amostra).HasColumnName("Id_Teste_Procedimento_Amostra").IsRequired();
            constuctor.HasKey(o => o.Id_Teste_Procedimento_Amostra); 
            constuctor.Property(m => m.Id_Teste_Procedimento).HasColumnName("Id_Teste_Procedimento"); 
            constuctor.Property(m => m.Teste_Procedimento_Amostra_Nome).HasColumnName("Teste_Procedimento_Amostra_Nome");
            constuctor.Property(m => m.Teste_Procedimento_Amostra_Descricao).HasColumnName("Teste_Procedimento_Amostra_Descricao");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
