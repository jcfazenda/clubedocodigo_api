﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteProcedimentoAnexoMap : IEntityTypeConfiguration<TesteProcedimentoAnexo>
    {

        public void Configure(EntityTypeBuilder<TesteProcedimentoAnexo> constuctor)
        {

            constuctor.ToTable("teste_procedimento_anexo");

            constuctor.Property(m => m.Id_Teste_Procedimento_Anexo).HasColumnName("Id_Teste_Procedimento_Anexo").IsRequired();
            constuctor.HasKey(o => o.Id_Teste_Procedimento_Anexo);
             
            constuctor.Property(m => m.Id_Teste_Procedimento).HasColumnName("Id_Teste_Procedimento");

            constuctor.Property(m => m.Teste_Procedimento_Anexo_Nome).HasColumnName("Teste_Procedimento_Anexo_Nome");
            constuctor.Property(m => m.Teste_Procedimento_Anexo_Byte).HasColumnName("Teste_Procedimento_Anexo_Byte"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
             
        }
    }
}
