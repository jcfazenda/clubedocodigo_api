﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteProcedimentoMap : IEntityTypeConfiguration<TesteProcedimento>
    {

        public void Configure(EntityTypeBuilder<TesteProcedimento> constuctor)
        {

            constuctor.ToTable("teste_procedimento");

            constuctor.Property(m => m.Id_Teste_Procedimento).HasColumnName("Id_Teste_Procedimento").IsRequired();
            constuctor.HasKey(o => o.Id_Teste_Procedimento);

            constuctor.Property(m => m.Id_Teste_Procedimento_Tipo).HasColumnName("Id_Teste_Procedimento_Tipo"); 
            constuctor.Property(m => m.Id_Teste).HasColumnName("Id_Teste");

            constuctor.Property(m => m.Teste_Procedimento_Nome).HasColumnName("Teste_Procedimento_Nome");
            constuctor.Property(m => m.Teste_Procedimento_Descricao).HasColumnName("Teste_Procedimento_Descricao");
            constuctor.Property(m => m.Data_Inicio).HasColumnName("Data_Inicio");
            constuctor.Property(m => m.Data_Fim).HasColumnName("Data_Fim");
            constuctor.Property(m => m.Fl_Efetivo).HasColumnName("Fl_Efetivo");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

            constuctor.HasOne(m => m.TesteProcedimentoTipo).WithMany(m => m.TesteProcedimento)
                      .HasForeignKey(x => x.Id_Teste_Procedimento_Tipo).HasPrincipalKey(x => x.Id_Teste_Procedimento_Tipo); /* Join */

        }
    }
}
