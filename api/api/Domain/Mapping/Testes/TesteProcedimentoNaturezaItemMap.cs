﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteProcedimentoNaturezaItemMap : IEntityTypeConfiguration<TesteProcedimentoNaturezaItem>
    {

        public void Configure(EntityTypeBuilder<TesteProcedimentoNaturezaItem> constuctor)
        {

            constuctor.ToTable("teste_procedimento_natureza_item");

            constuctor.Property(m => m.Id_Teste_Procedimento_Natureza_Item).HasColumnName("Id_Teste_Procedimento_Natureza_Item").IsRequired();
            constuctor.HasKey(o => o.Id_Teste_Procedimento_Natureza_Item);

            constuctor.Property(m => m.Id_Teste_Procedimento_Natureza_Item).HasColumnName("Id_Teste_Procedimento_Natureza_Item");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
