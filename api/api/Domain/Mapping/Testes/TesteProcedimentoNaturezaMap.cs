﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteProcedimentoNaturezaMap : IEntityTypeConfiguration<TesteProcedimentoNatureza>
    {

        public void Configure(EntityTypeBuilder<TesteProcedimentoNatureza> constuctor)
        {

            constuctor.ToTable("teste_procedimento_natureza");

            constuctor.Property(m => m.Id_Teste_Procedimento_Natureza).HasColumnName("Id_Teste_Procedimento_Natureza").IsRequired();
            constuctor.HasKey(o => o.Id_Teste_Procedimento_Natureza);

            constuctor.Property(m => m.Id_Teste_Procedimento).HasColumnName("Id_Teste_Procedimento");  
            constuctor.Property(m => m.Id_Teste_Procedimento_Natureza_Item).HasColumnName("Id_Teste_Procedimento_Natureza_Item"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo"); 
        }
    }
}
