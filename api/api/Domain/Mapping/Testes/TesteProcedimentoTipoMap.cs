﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteProcedimentoTipoMap : IEntityTypeConfiguration<TesteProcedimentoTipo>
    {

        public void Configure(EntityTypeBuilder<TesteProcedimentoTipo> constuctor)
        {

            constuctor.ToTable("teste_procedimento_tipo");

            constuctor.Property(m => m.Id_Teste_Procedimento_Tipo).HasColumnName("Id_Teste_Procedimento_Tipo").IsRequired();
            constuctor.HasKey(o => o.Id_Teste_Procedimento_Tipo);

            constuctor.Property(m => m.Teste_Procedimento_Tipo_Nome).HasColumnName("Teste_Procedimento_Tipo_Nome"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo"); 

        }
    }
}
