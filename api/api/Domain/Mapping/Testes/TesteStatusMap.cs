﻿using api.Domain.Models.Testes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.Testes
{
    public sealed class TesteStatusMap : IEntityTypeConfiguration<TesteStatus>
    {

        public void Configure(EntityTypeBuilder<TesteStatus> constuctor)
        {

            constuctor.ToTable("teste_status");

            constuctor.Property(m => m.Id_Teste_Status).HasColumnName("Id_Teste_Status").IsRequired();
            constuctor.HasKey(o => o.Id_Teste_Status);

            constuctor.Property(m => m.Teste_Status_Nome).HasColumnName("Teste_Status_Nome"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
            constuctor.Property(m => m.Fl_Encerrado).HasColumnName("Fl_Encerrado");
            constuctor.Property(m => m.Fl_Finalizado).HasColumnName("Fl_Finalizado");
            constuctor.Property(m => m.Icone).HasColumnName("Icone");

        }
    }
}
