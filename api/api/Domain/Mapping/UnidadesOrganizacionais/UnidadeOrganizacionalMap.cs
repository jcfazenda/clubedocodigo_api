﻿using api.Domain.Models.UnidadesOrganizacionais;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.UnidadesOrganizacionais
{
    public sealed class UnidadeOrganizacionalMap : IEntityTypeConfiguration<UnidadeOrganizacional>
    {

        public void Configure(EntityTypeBuilder<UnidadeOrganizacional> constuctor)
        {

            constuctor.ToTable("unidade_organizacional");


            constuctor.Property(m => m.Id_Unidade_Organizacional).HasColumnName("Id_Unidade_Organizacional").IsRequired();
            constuctor.HasKey(o => o.Id_Unidade_Organizacional);

            constuctor.Property(m => m.Unidade_Organizacional_Nome).HasColumnName("Unidade_Organizacional_Nome");
            constuctor.Property(m => m.Id_Usuario).HasColumnName("Id_Usuario");
            constuctor.Property(m => m.Id_Unidade_Organizacional_Orgao).HasColumnName("Id_Unidade_Organizacional_Orgao");
            constuctor.Property(m => m.Id_Unidade_Organizacional_Responsabilidade).HasColumnName("Id_Unidade_Organizacional_Responsabilidade");
            constuctor.Property(m => m.Unidade_Organizacional_Nome).HasColumnName("Unidade_Organizacional_Nome");
            constuctor.Property(m => m.Unidade_Organizacional_Descricao).HasColumnName("Unidade_Organizacional_Descricao");

            constuctor.Property(m => m.Data_Inicio).HasColumnName("Data_Inicio");
            constuctor.Property(m => m.Data_Fim).HasColumnName("Data_Fim");

            constuctor.Property(m => m.Fl_Temporaria).HasColumnName("Fl_Temporaria");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");

        }
    }
}
