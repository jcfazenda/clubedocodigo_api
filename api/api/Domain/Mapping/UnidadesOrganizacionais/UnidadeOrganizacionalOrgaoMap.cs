﻿using api.Domain.Models.UnidadesOrganizacionais;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.UnidadesOrganizacionais
{
    public sealed class UnidadeOrganizacionalOrgaoMap : IEntityTypeConfiguration<UnidadeOrganizacionalOrgao>
    {

        public void Configure(EntityTypeBuilder<UnidadeOrganizacionalOrgao> constuctor)
        {

            constuctor.ToTable("unidade_organizacional_orgao"); 

            constuctor.Property(m => m.Id_Unidade_Organizacional_Orgao).HasColumnName("Id_Unidade_Organizacional_Orgao").IsRequired();
            constuctor.HasKey(o => o.Id_Unidade_Organizacional_Orgao);

            constuctor.Property(m => m.Unidade_Organizacional_Orgao_Nome).HasColumnName("Unidade_Organizacional_Orgao_Nome"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo"); 
        }
    }
}
