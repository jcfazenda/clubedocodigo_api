﻿using api.Domain.Models.UnidadesOrganizacionais;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.UnidadesOrganizacionais
{
    public sealed class UnidadeOrganizacionalResponsabilidadeMap : IEntityTypeConfiguration<UnidadeOrganizacionalResponsabilidade>
    {

        public void Configure(EntityTypeBuilder<UnidadeOrganizacionalResponsabilidade> constuctor)
        {

            constuctor.ToTable("unidade_organizacional_responsabilidade");

            constuctor.Property(m => m.Id_Unidade_Organizacional_Responsabilidade).HasColumnName("Id_Unidade_Organizacional_Responsabilidade").IsRequired();
            constuctor.HasKey(o => o.Id_Unidade_Organizacional_Responsabilidade);

            constuctor.Property(m => m.Unidade_Organizacional_Responsabilidade_Nome).HasColumnName("Unidade_Organizacional_Responsabilidade_Nome");
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
        }
    }
}
