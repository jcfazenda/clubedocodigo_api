﻿using api.Domain.Models.WorkFlow;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace api.Domain.Mapping.WorkFlow
{
    public sealed class WorkFlowStatusMap : IEntityTypeConfiguration<WorkFlowStatus>
    {

        public void Configure(EntityTypeBuilder<WorkFlowStatus> constuctor)
        {

            constuctor.ToTable("workflow_status");

            constuctor.Property(m => m.Id_Workflow_Status).HasColumnName("Id_Workflow_Status").IsRequired();
            constuctor.HasKey(o => o.Id_Workflow_Status);

            constuctor.Property(m => m.Workflow_Status_Nome).HasColumnName("Workflow_Status_Nome"); 
            constuctor.Property(m => m.Fl_Ativo).HasColumnName("Fl_Ativo");
            constuctor.Property(m => m.Fl_Encerrado).HasColumnName("Fl_Encerrado");
            constuctor.Property(m => m.Fl_Finalizado).HasColumnName("Fl_Finalizado");
            constuctor.Property(m => m.Icone).HasColumnName("Icone");


        }
    }
}
