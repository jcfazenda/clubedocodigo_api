﻿namespace api.Domain.Models.Materia
{
    public class Materias
    {
        public Materias()
        {
        }

        public Materias(long id_Materia, string materia_Nome, bool? fl_Ativo)
        {
            Id_Materia = id_Materia;
            Materia_Nome = materia_Nome;
            Fl_Ativo = fl_Ativo;
        }

        public long Id_Materia { get; set; }
        public string Materia_Nome { get; set; }
        public bool? Fl_Ativo { get; set; }
    }
}
