﻿using System.Linq;
using api.Domain.Models.Materia;
using api.Domain.Views.Input.Materia;

namespace api.Domain.Repository.Interface.Materiass
{
    public interface IMateriasRepository : IRepository<Materias, decimal>
    {
        IQueryable<Materias> GetAny(bool active);
        IQueryable<Materias> GetById(long id);

        bool UpdateStatus(long id);
        long Create(MateriasInput input);
        bool Update(MateriasInput input);
        bool Remove(long id);

    }
}
