﻿using System.Linq;
using api.Domain.Models.Materia;
using api.Domain.Repository.Interface.Materiass;
using api.Domain.Views.Input.Materia;

namespace api.Domain.Repository.Queryable.Materiass
{
    public class MateriasRepository : Repository<Materias, decimal>, IMateriasRepository
    {
        private readonly GRCContext _context;
        public MateriasRepository(GRCContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<Materias> GetAny(bool active)
        {
            var data = DbSet.Where(x => x.Fl_Ativo.Equals(active)).AsQueryable();

            return data;
        }

        public IQueryable<Materias> GetById(long id)
        {
            var data = DbSet.Where(x => x.Id_Materia.Equals(id)).AsQueryable();

            return data;
        }

        public bool UpdateStatus(long id)
        {
            Materias data = DbSet.Where(x => x.Id_Materia.Equals(id)).AsQueryable().FirstOrDefault();

            data.Fl_Ativo = data.Fl_Ativo == true ? false : true;

            _context.Update(data);
            _context.SaveChanges();

            return true;
        }

        public long Create(MateriasInput input)
        {
            Materias data = new Materias
            {
                Materia_Nome = input.Materia_Nome,
                Fl_Ativo     = true
            };

            _context.Add(data);
            _context.SaveChanges();

            return data.Id_Materia;
        }

        public bool Update(MateriasInput input)
        {
            Materias data = DbSet.Where(x => x.Id_Materia.Equals(input.Id_Materia)).AsQueryable().FirstOrDefault();

            data.Materia_Nome = input.Materia_Nome;

            _context.Update(data);
            _context.SaveChanges();

            return true;
        }
        public bool Remove(long id)
        {
            Materias data = DbSet.Where(x => x.Id_Materia.Equals(id)).AsQueryable().FirstOrDefault();

            _context.Remove(data);
            _context.SaveChanges();

            return true;
        }


    }
}
