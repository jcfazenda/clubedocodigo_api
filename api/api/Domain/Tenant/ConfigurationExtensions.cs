﻿using Microsoft.Extensions.Configuration;

namespace api.Domain.Tenant
{
    public static class ConfigurationExtensions
    {
        public static string GetClientConnectionString(this IConfiguration configuration, string clientName)
        {
            /* QA - Desenvolvimento - Homologação - produção */
            string[] item     = clientName.Split('-');
            string connString = "";

            var Connection = new[] {
                    new {  IP       = "localhost",
                           Password = "root",
                           User     = "root"  }
                };


            connString = configuration.GetConnectionString("conn").Replace("__DBNAME__", item[0])
                                                                  .Replace("__IP__", Connection[0].IP)
                                                                  .Replace("__USER__", Connection[0].User)
                                                                  .Replace("__PASSWORD__", Connection[0].Password);

            return connString;
        }
    }
} 