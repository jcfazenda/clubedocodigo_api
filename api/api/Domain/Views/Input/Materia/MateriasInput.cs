﻿namespace api.Domain.Views.Input.Materia
{
    public class MateriasInput
    {
        public long Id_Materia { get; set; }
        public string Materia_Nome { get; set; }
        public bool? Fl_Ativo { get; set; }

        public string Rota { get; set; }
    }
}
