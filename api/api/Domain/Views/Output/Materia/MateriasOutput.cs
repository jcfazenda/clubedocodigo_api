﻿using System;
namespace api.Domain.Views.Output.Materia
{
	public class MateriasOutput
	{
        public long Id_Materia { get; set; }
        public string Materia_Nome { get; set; }
        public bool? Fl_Ativo { get; set; }
    }
}

