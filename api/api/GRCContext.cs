﻿using api.Domain.Mapping.Estados;
using Microsoft.EntityFrameworkCore;

namespace api
{
    public class GRCContext : DbContext
    {
        public GRCContext()
        {

        }

        public GRCContext(DbContextOptions options)
        : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /* Materias */
            modelBuilder.ApplyConfiguration(new MateriasMap());

            base.OnModelCreating(modelBuilder);
        }
         
    }
}
