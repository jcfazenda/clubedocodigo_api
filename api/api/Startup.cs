﻿
using api.Domain.Configure;
using api.Domain.Models;
using api.Domain.Tenant;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            /* Authentication */
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<AuthenticatedUser>();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            }); 
             
            services.AddAutoMapper();
            RegisterServices(services);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddMvc().AddJsonOptions(options => {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            }
            );

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });

            services.AddOptions();
            services.AddSignalR(); /* HUB SignalR */

            /* TENANT */
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddCustomerDbContext(Configuration);
        }

        public void Configure(IApplicationBuilder app)
        {

            app.UseAuthentication();
            app.UseMvc();
            app.UseCookiePolicy();

            app.UseCors("CorsPolicy");
            app.UseCors(config => config.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());

            /* HUB SignalR */
            app.UseSignalR(routes =>
            {
                routes.MapHub<SignalR>("/SignalR");
            });

        }

        private static void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton(typeof(CacheSegments));
            NativeInjector.RegisterServices(services);
        }
    }
}